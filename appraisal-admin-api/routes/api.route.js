var express = require('express')

var router = express.Router()
var roles = require('./api/role.route');
var functional_list = require('./api/functional_list.route');
var goalAttribute = require('./api/goal_attribute.route');
var goalCategory = require('./api/goal_category.route');
var goalSubcategory = require('./api/goal_subcategory.route');
var matrixLookForLastYear = require('./api/matric_look_for_last_year.route');
var appraisalFunctionalAttribute = require('./api/appraisal_functional_attributes.route');

var competenciesAttribute = require('./api/competencies_attribute.route');
var competenciesFunctionalAttributes = require('./api/competencies_functional_attributes.route');
var generalConfig = require('./api/general_config.route');
var competenciesLevel = require('./api/competencies_level.route');

var appraisalMasterRoute = require('./api/appraisal.master.route');
var appraisalUserRoute = require('./api/appraisal.user.route');
var developmentPlanRoute = require('./api/development_plan.route');

var reviewOfGoalsRoute = require('./api/review_of_goals.route');
var reviewOfGoalsTargetRoute = require('./api/review_of_goals_target.route');
var reviewOfCompetenciesRoute = require('./api/review_of_competencies.route');
var reviewOfCompetenciesLevelRoute = require('./api/review_of_competencies_level.route');

var home = require('./api/home.route');

router.use('/roles', roles);
router.use('/functional-list', functional_list);
router.use('/goal-attribute', goalAttribute);
router.use('/goal-category', goalCategory);
router.use('/goal-subcategory', goalSubcategory);
router.use('/matrix-look-for-last-year', matrixLookForLastYear);
router.use('/appraisal-functional-attribute', appraisalFunctionalAttribute);

router.use('/appraisal', appraisalMasterRoute);
router.use('/appraisal-user', appraisalUserRoute);

router.use('/competencies-attribute', competenciesAttribute);
router.use('/competencies-functional-attributes', competenciesFunctionalAttributes);
router.use('/general-config', generalConfig);
router.use('/development-plan',developmentPlanRoute)

router.use('/home', home);
router.use('/competencies_level', competenciesLevel);

router.use('/review-of-goals', reviewOfGoalsRoute);
router.use('/review-of-goals-target', reviewOfGoalsTargetRoute);
router.use('/review-of-competencies', reviewOfCompetenciesRoute);
router.use('/review-of-competencies-level', reviewOfCompetenciesLevelRoute);
module.exports = router;