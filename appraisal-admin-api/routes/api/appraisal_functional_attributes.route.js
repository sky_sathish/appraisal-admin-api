var express = require('express')

var router = express.Router()

// Getting the ApprisalFunctionalAttribute Controller that we just created

var ApprisalFunctionalAttributeController = require('../../controllers/appraisal_functional_attributes.controller');


// Map each API to the Controller FUnctions

router.get('/', ApprisalFunctionalAttributeController.getApprisalFunctionalAttributes)

router.post('/', ApprisalFunctionalAttributeController.createApprisalFunctionalAttribute)

router.put('/', ApprisalFunctionalAttributeController.updateApprisalFunctionalAttribute)

router.delete('/:id',ApprisalFunctionalAttributeController.removeApprisalFunctionalAttribute)


// Export the Router

module.exports = router;