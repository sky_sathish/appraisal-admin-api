var express = require('express')
var router = express.Router()

// Getting the ReviewOfGoals Controller that we just created
var ReviewOfCompetenciesLevelController = require('../../controllers/review_of_competencies_level.controller');


// Map each API to the Controller FUnctions
router.post('/', ReviewOfCompetenciesLevelController.createReviewOfCompetenciesLevel)

router.put('/', ReviewOfCompetenciesLevelController.updateReviewOfCompetenciesLevel)

router.delete('/:id',ReviewOfCompetenciesLevelController.removeReviewOfCompetenciesLevel)

// Export the Router
module.exports = router;