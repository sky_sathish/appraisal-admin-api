var express = require('express')

var router = express.Router()

// Getting the GoalAttribute Controller that we just created

var CompetenciesLevelController = require('../../controllers/competencies_level.controller');


// Map each API to the Controller FUnctions

router.get('/', CompetenciesLevelController.getCompetenciesLevel)

router.post('/', CompetenciesLevelController.createCompetenciesLevel)

router.put('/', CompetenciesLevelController.updateCompetenciesLevel)

router.delete('/:id',CompetenciesLevelController.removeCompetenciesLevel)


// Export the Router

module.exports = router;