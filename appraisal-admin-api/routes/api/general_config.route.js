var express = require('express')

var router = express.Router()

// Getting the GeneralConfig Controller that we just created

var GeneralConfigController = require('../../controllers/general_config.controller');


// Map each API to the Controller FUnctions

router.get('/', GeneralConfigController.getGeneralConfigs)

router.post('/', GeneralConfigController.createGeneralConfig)

router.put('/', GeneralConfigController.updateGeneralConfig)

router.delete('/:id',GeneralConfigController.removeGeneralConfig)


// Export the Router

module.exports = router;