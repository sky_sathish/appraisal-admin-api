var express = require('express')

var router = express.Router()

// Getting the GoalCategory Controller that we just created

var GoalCategoryController = require('../../controllers/goal_category.controller');


// Map each API to the Controller FUnctions

router.get('/', GoalCategoryController.getGoalCategorys)

router.post('/', GoalCategoryController.createGoalCategory)

router.put('/', GoalCategoryController.updateGoalCategory)

router.delete('/:id',GoalCategoryController.removeGoalCategory)


// Export the Router

module.exports = router;