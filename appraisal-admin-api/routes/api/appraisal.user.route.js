var express = require('express')

var router = express.Router()

var AppraisalUserController = require('../../controllers/appraisal.user.controller');

router.get('/', AppraisalUserController.getAppraisalUser);

router.get('/children', AppraisalUserController.getAppraisalChild);

router.post('/', AppraisalUserController.createAppraisalUser);

router.put('/', AppraisalUserController.updateAppraisalUser);

router.delete('/:id', AppraisalUserController.removeAppraisalUser);

module.exports = router;