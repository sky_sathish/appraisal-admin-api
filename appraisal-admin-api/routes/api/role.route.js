var express = require('express')

var router = express.Router()

// Getting the Role Controller that we just created

var RoleController = require('../../controllers/roles.controller');


// Map each API to the Controller FUnctions

router.get('/', RoleController.getRoles)

router.get('/q', RoleController.getRolesByName)

router.post('/', RoleController.createRole)

router.post('/auto', RoleController.autoCreateRole)

router.put('/', RoleController.updateRole)

router.delete('/:id',RoleController.removeRole)


// Export the Router

module.exports = router;