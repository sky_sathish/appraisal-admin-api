var express = require('express')

var router = express.Router()

var AppraisalMasterController = require('../../controllers/appraisal.master.controller.js');

router.get('/', AppraisalMasterController.getAppraisalMaster);

router.post('/', AppraisalMasterController.createAppraisalMaster);

router.put('/', AppraisalMasterController.updateAppraisalMaster);

router.delete('/:id', AppraisalMasterController.removeAppraisalMaster);


// Export the Router

module.exports = router;