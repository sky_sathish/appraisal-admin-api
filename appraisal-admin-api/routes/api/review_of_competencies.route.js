var express = require('express')
var router = express.Router()

// Getting the ReviewOfGoals Controller that we just created
var ReviewOfCompetenciesController = require('../../controllers/review_of_competencies.controller');


// Map each API to the Controller FUnctions
router.get('/', ReviewOfCompetenciesController.getReviewOfCompetencies)

router.post('/', ReviewOfCompetenciesController.createReviewOfCompetencies)

router.put('/', ReviewOfCompetenciesController.updateReviewOfCompetencies)

router.delete('/:id',ReviewOfCompetenciesController.removeReviewOfCompetencies)

router.post('/clone', ReviewOfCompetenciesController.cloneReviewOfCompetencies)
// Export the Router
module.exports = router;