var express = require('express')

var router = express.Router()

// Getting the GoalSubCategory Controller that we just created

var GoalSubCategoryController = require('../../controllers/goal_subcategory.controller');


// Map each API to the Controller FUnctions

router.get('/', GoalSubCategoryController.getGoalSubCategorys)

router.post('/', GoalSubCategoryController.createGoalSubCategory)

router.put('/', GoalSubCategoryController.updateGoalSubCategory)

router.delete('/:id',GoalSubCategoryController.removeGoalSubCategory)


// Export the Router

module.exports = router;