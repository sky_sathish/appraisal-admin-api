var express = require('express')
var router = express.Router()

// Getting the ReviewOfGoals Controller that we just created
var ReviewOfGoalsController = require('../../controllers/review_of_goals.controller');


// Map each API to the Controller FUnctions
router.get('/', ReviewOfGoalsController.getReviewOfGoals)

router.post('/', ReviewOfGoalsController.createReviewOfGoals)

router.put('/', ReviewOfGoalsController.updateReviewOfGoals)

router.delete('/:id',ReviewOfGoalsController.removeReviewOfGoals)

router.post('/clone', ReviewOfGoalsController.cloneReviewOfGoals)

// Export the Router
module.exports = router;