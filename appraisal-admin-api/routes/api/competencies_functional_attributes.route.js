var express = require('express')

var router = express.Router()

// Getting the CompetenciesFunctionalAttribute Controller that we just created

var CompetenciesFunctionalAttributeController = require('../../controllers/competencies_functional_attributes.controller');


// Map each API to the Controller FUnctions

router.get('/', CompetenciesFunctionalAttributeController.getCompetenciesFunctionalAttributes)

router.post('/', CompetenciesFunctionalAttributeController.createCompetenciesFunctionalAttribute)

router.put('/', CompetenciesFunctionalAttributeController.updateCompetenciesFunctionalAttribute)

router.delete('/:id',CompetenciesFunctionalAttributeController.removeCompetenciesFunctionalAttribute)


// Export the Router

module.exports = router;