var express = require('express')

var router = express.Router()

// Getting the MatrixLookForLastYear Controller that we just created

var MatrixLookForLastYearController = require('../../controllers/matric_look_for_last_year.controller');


// Map each API to the Controller FUnctions

router.get('/', MatrixLookForLastYearController.getMatrixLookForLastYears)

router.post('/', MatrixLookForLastYearController.createMatrixLookForLastYear)

router.put('/', MatrixLookForLastYearController.updateMatrixLookForLastYear)

router.delete('/:id',MatrixLookForLastYearController.removeMatrixLookForLastYear)


// Export the Router

module.exports = router;