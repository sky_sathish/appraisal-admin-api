var express = require('express')

var router = express.Router()

// Getting the Home Controller that we just created

var HomeController = require('../../controllers/home.controller');


// Map each API to the Controller FUnctions

router.get('/', HomeController.getHomes)

router.post('/', HomeController.createHome)

router.put('/', HomeController.updateHome)

router.delete('/:id',HomeController.removeHome)


// Export the Router

module.exports = router;