var express = require('express')
var router = express.Router()
var DevelopmentPlanController = require('../../controllers/development_plan.controller');

router.get('/', DevelopmentPlanController.getDevelopmentPlan)

router.post('/', DevelopmentPlanController.createDevelopmentPlan)

router.put('/', DevelopmentPlanController.updateDevelopmentPlan)

router.delete('/:id',DevelopmentPlanController.removeDevelopmentPlan)

module.exports = router;