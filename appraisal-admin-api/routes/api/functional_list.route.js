var express = require('express')

var router = express.Router()

// Getting the FunctionalList Controller that we just created

var FunctionalListController = require('../../controllers/functional_list.controller');


// Map each API to the Controller FUnctions

router.get('/', FunctionalListController.getFunctionalList)

router.get('/q', FunctionalListController.getFunctionalListByName)

router.post('/', FunctionalListController.createFunctionalList)

router.post('/auto', FunctionalListController.autoCreateFunctionalList)

router.put('/', FunctionalListController.updateFunctionalList)

router.delete('/:id',FunctionalListController.removeFunctionalList)


// Export the Router

module.exports = router;