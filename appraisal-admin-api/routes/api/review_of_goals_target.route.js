var express = require('express')
var router = express.Router()

// Getting the ReviewOfGoals Controller that we just created
var ReviewOfGoalsTargetController = require('../../controllers/review_of_goals_target.controller');


// Map each API to the Controller FUnctions
router.post('/', ReviewOfGoalsTargetController.createReviewOfGoalsTarget)

router.put('/', ReviewOfGoalsTargetController.updateReviewOfGoalsTarget)

router.delete('/:id',ReviewOfGoalsTargetController.removeReviewOfGoalsTarget)

// Export the Router
module.exports = router;