var express = require('express')

var router = express.Router()

// Getting the GoalAttribute Controller that we just created

var GoalAttributeController = require('../../controllers/goal_attribute.controller');


// Map each API to the Controller FUnctions

router.get('/', GoalAttributeController.getGoalAttributes)

router.post('/', GoalAttributeController.createGoalAttribute)

router.put('/', GoalAttributeController.updateGoalAttribute)

router.delete('/:id',GoalAttributeController.removeGoalAttribute)


// Export the Router

module.exports = router;