var express = require('express')

var router = express.Router()

// Getting the CompetenciesAttribute Controller that we just created

var CompetenciesAttributeController = require('../../controllers/competencies_attributes.controller');


// Map each API to the Controller FUnctions

router.get('/', CompetenciesAttributeController.getCompetenciesAttributes)

router.post('/', CompetenciesAttributeController.createCompetenciesAttribute)

router.put('/', CompetenciesAttributeController.updateCompetenciesAttribute)

router.delete('/:id',CompetenciesAttributeController.removeCompetenciesAttribute)


// Export the Router

module.exports = router;