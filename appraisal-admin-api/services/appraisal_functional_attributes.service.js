var ApprisalFunctionalAttribute = require('../models/appraisal_functional_attributes.model')

// Saving the context of this module inside the _the variable
_this = this

// Async function to get the To do List
exports.getApprisalFunctionalAttributes = async function(query, page, limit){
    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    // Try Catch the awaited promise to handle the error 
    try {
        var apprisalFunctionalAttributes = await ApprisalFunctionalAttribute.find({})
        .populate("category")
        .populate("subcategory")
        .populate("role_id");
        // Return the apprisalFunctionalAttributed list that was retured by the mongoose promise
        return apprisalFunctionalAttributes;
    } catch (e) {
        // return a Error message describing the reason 
        throw Error('Error while Paginating ApprisalFunctionalAttributes')
    }
}

// Async function to get the To do List
exports.getApprisalFunctionalAttributesById = async function(id,query, page, limit){
    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    // Try Catch the awaited promise to handle the error 
    try {
        var apprisalFunctionalAttributes = await ApprisalFunctionalAttribute.findById(id)
        .populate("category")
        .populate("subcategory")
        .populate("role_id");

        if(apprisalFunctionalAttributes!=undefined){
            return apprisalFunctionalAttributes;
        }else{
            return "Data not found";
        }
        // Return the apprisalFunctionalAttributed list that was retured by the mongoose promise
    } catch (e) {
        // return a Error message describing the reason 
        throw Error('Error while Paginating ApprisalFunctionalAttributes')
    }
}

// Async function to get the To do List
exports.getApprisalFunctionalAttributesByRoleAndFunctionID = async function(function_id,role_id,query, page, limit){
    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    // Try Catch the awaited promise to handle the error 
    try {
        var apprisalFunctionalAttributes = await ApprisalFunctionalAttribute.find({
            "function_id":function_id,
            "role_id" : role_id
        })
        .populate("category")
        .populate("subcategory")
        .populate("role_id");

        if(apprisalFunctionalAttributes!=undefined && apprisalFunctionalAttributes.length>0){
            return apprisalFunctionalAttributes;
        }else{
            return "Data not found";
        }
        // Return the apprisalFunctionalAttributed list that was retured by the mongoose promise
    } catch (e) {
        // return a Error message describing the reason 
        throw Error('Error while Paginating ApprisalFunctionalAttributes')
    }
}

exports.createApprisalFunctionalAttribute = async function(data){
    // Creating a new Mongoose Object by using the new keyword
    var newApprisalFunctionalAttribute = new ApprisalFunctionalAttribute({
        function_id:data.function_id,
        role_id:data.role_id,
        category:data.category,
        subcategory:data.subcategory,
        description:data.description,
        target_for_last_year:data.target_for_last_year,
        target_for_last_year_unit:data.target_for_last_year_unit,
        target_for_last_year_description:data.target_for_last_year_description,
        target_for_next_year:data.target_for_next_year,
        target_for_next_year_unit:data.target_for_next_year_unit,
        target_for_next_year_description:data.target_for_next_year_description,
        weightage:data.weightage
    })

    try{
        // Saving the ApprisalFunctionalAttribute 
        var savedApprisalFunctionalAttribute = await newApprisalFunctionalAttribute.save()
        return savedApprisalFunctionalAttribute;
    }catch(e){
        // return a Error message describing the reason     
        throw Error("Error while Creating ApprisalFunctionalAttribute:"+e.message);
    }
}

exports.updateApprisalFunctionalAttribute = async function(data){
    var id = data._id
    try{
        //Find the old ApprisalFunctionalAttribute Object by the Id
        var oldApprisalFunctionalAttributeData = await ApprisalFunctionalAttribute.findById(id);
    }catch(e){
        throw Error("Error occured while Finding the ApprisalFunctionalAttribute")
    }
    // If no old ApprisalFunctionalAttribute Object exists return false
    if(!oldApprisalFunctionalAttributeData){
        return false;
    }
    //Edit the ApprisalFunctionalAttribute Object
    oldApprisalFunctionalAttributeData.function_id=data.function_id
    oldApprisalFunctionalAttributeData.role_id=data.role_id ? data.role_id :null
    oldApprisalFunctionalAttributeData.category=data.category ? data.category : null
    oldApprisalFunctionalAttributeData.subcategory=data.subcategory ? data.subcategory : null
    oldApprisalFunctionalAttributeData.description=data.description ? data.description : null
    oldApprisalFunctionalAttributeData.target_for_last_year=data.target_for_last_year ? data.target_for_last_year : null
    oldApprisalFunctionalAttributeData.target_for_last_year_unit=data.target_for_last_year_unit ? data.target_for_last_year_unit : null
    oldApprisalFunctionalAttributeData.target_for_last_year_description=data.target_for_last_year_description ? data.target_for_last_year_description : null
    oldApprisalFunctionalAttributeData.target_for_next_year=data.target_for_next_year ? data.target_for_next_year : null
    oldApprisalFunctionalAttributeData.target_for_next_year_unit=data.target_for_next_year_unit ? data.target_for_next_year_unit : null
    oldApprisalFunctionalAttributeData.target_for_next_year_description=data.target_for_next_year_description ? data.target_for_next_year_description : null
    oldApprisalFunctionalAttributeData.weightage=data.weightage ? data.weightage : null
    
    try{
        var savedApprisalFunctionalAttribute = await oldApprisalFunctionalAttributeData.save()
        return savedApprisalFunctionalAttribute;
    }catch(e){
        throw Error("And Error occured while updating the ApprisalFunctionalAttribute:"+e.message);
    }
}

exports.deleteApprisalFunctionalAttribute = async function(id){
    // Delete the ApprisalFunctionalAttribute
    try{
        var deleted = await ApprisalFunctionalAttribute.remove({_id: id})
        if(deleted.n === 0){
            throw Error("ApprisalFunctionalAttribute Could not be deleted")
        }
        return deleted
    }catch(e){
        throw Error("Error Occured while Deleting the ApprisalFunctionalAttribute")
    }
}