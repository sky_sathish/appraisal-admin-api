var GoalAttribute = require('../models/goal_attribute.model')

// Saving the context of this module inside the _the variable
_this = this

// Async function to get the To do List
exports.getGoalAttributes = async function(query, page, limit){

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var goalAttributes = await GoalAttribute.find({})
        .populate('attribute_category').populate('attribute_sub_category')
        
        // Return the goalAttributesd list that was retured by the mongoose promise
        return goalAttributes;

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating GoalAttributes'+e)
    }
}
exports.getGoalAttributesById = async function(id,query, page, limit){

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var goalAttributes = await GoalAttribute.findById(id)
        .populate('attribute_category').populate('attribute_sub_category');
        if(goalAttributes!=undefined){
            return goalAttributes;
        }else{
            return "Data not found";
        }
        // Return the goalAttributesd list that was retured by the mongoose promise
       
    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating GoalAttributes'+e)
    }
}

exports.createGoalAttribute = async function(goalAttribute){
    
    // Creating a new Mongoose Object by using the new keyword
    var newGoalAttribute = new GoalAttribute({
        attribute:goalAttribute.attribute,
        attribute_sub_category:goalAttribute.attribute_sub_category,
        attribute_category:goalAttribute.attribute_category
    })

    try{

        // Saving the GoalAttributes 
        var savedGoalAttribute = await newGoalAttribute.save()

        return savedGoalAttribute;
    }catch(e){
      
        // return a Error message describing the reason     
        throw Error("Error while Creating GoalAttributes:"+e.message);
    }
}

exports.updateGoalAttribute = async function(goalAttribute){
    var id = goalAttribute.id

    try{
        //Find the old GoalAttributes Object by the Id
    
        var oldGoalAttribute = await GoalAttribute.findById(id);
    }catch(e){
        throw Error("Error occured while Finding the GoalAttributes")
    }

    // If no old GoalAttributes Object exists return false
    if(!oldGoalAttribute){
        return false;
    }

    console.log(oldGoalAttribute)

    //Edit the GoalAttributes Object

    oldGoalAttribute.attribute=goalAttribute.attribute
    oldGoalAttribute.attribute_sub_category=goalAttribute.attribute_sub_category
    oldGoalAttribute.attribute_category=goalAttribute.attribute_category


    console.log(oldGoalAttribute)

    try{
        var savedGoalAttribute = await oldGoalAttribute.save()
        return savedGoalAttribute;
    }catch(e){
        throw Error("And Error occured while updating the GoalAttributes:"+e.message);
    }
}

exports.deleteGoalAttribute = async function(id){
    
    // Delete the GoalAttributes
    try{
        var deleted = await GoalAttribute.remove({_id: id})
        if(deleted.n === 0){
            throw Error("GoalAttributes Could not be deleted")
        }
        return deleted
    }catch(e){
        throw Error("Error Occured while Deleting the GoalAttributes")
    }
}