var ReviewOfGoalsTargetModel = require('../models/review_of_goal_target.model')

var SAVE_ENTRY_CODE = 'save';
var UPDATE_ENTRY_CODE = 'update';
var DUPLICATE_CODE= 'duplicate';

exports.createReviewOfGoalsTarget = async function (data) {

    var status = await checkDuplicateEntryGoalsTarget(data,SAVE_ENTRY_CODE)
    console.log("createReviewOfGoalsTarget status -------> "+status)
    if( status != DUPLICATE_CODE){
    // Creating a new Mongoose Object by using the new keyword
    var newData = new ReviewOfGoalsTargetModel({
        review_goals_id: data.review_goals_id,
        target: data.target,
        weightage: data.weightage,
        year: data.year,
        term: data.term,
        active_status: true
    });
    try {
        // Saving the ReviewOfGoals 
        var savedData = await newData.save()
        return savedData;
    } catch (e) {
        // return a Error message describing the reason     
        throw Error("Error while Creating Review Of Goals :"+e.message)
    }
}else{
    return status;
}
}
exports.updateReviewOfGoalsTarget = async function (data) {
  
    var status = await checkDuplicateEntryGoalsTarget(data,UPDATE_ENTRY_CODE)
    console.log("updateReviewOfGoalsTarget status -------> "+status)
    if( status != DUPLICATE_CODE){
    var id = data._id
    try {
        //Find the old ReviewOfGoalsTargetModel Object by the Id
        var oldData = await ReviewOfGoalsTargetModel.findById(id);
    } catch (e) {
        throw Error("Error occured while Finding the ReviewOfGoals")
    }
    // If no old ReviewOfGoals Object exists return false
    if (!oldData) {
        return false;
    }
    //Edit the ReviewOfGoals Object
    oldData.review_goals_id = data.review_goals_id;
    oldData.target = data.target;
    oldData.weightage = data.weightage;
    oldData.year = data.year;
    oldData.term = data.term;
    oldData.active_status = data.active_status;
    try {
        var updatedData = await oldData.save()
        return updatedData;
    } catch (e) {
        throw Error("And Error occured while updating the ReviewOfGoals:"+e.message);
    }
    }else{
    return status;
    }
}

exports.deleteReviewOfGoalsTarget = async function (id) {

    // Delete the ReviewOfGoals
    try {
        var deleted = await ReviewOfGoalsTargetModel.remove({ _id: id })
        if (deleted.n === 0) {
            throw Error("ReviewOfGoalsTarget Could not be deleted")
        }
        return "success"
    } catch (e) {
        throw Error("Error Occured while Deleting the ReviewOfGoalsTarget")
    }
}

checkDuplicateEntryGoalsTarget = async function(data,action){
    var findData;
    var result;
    // check save data
    if(action == SAVE_ENTRY_CODE){
    findData = {
        review_goals_id:data.review_goals_id,
        year:data.year,
        term:data.term
    }
    result = await ReviewOfGoalsTargetModel.find(findData)
    console.log("result.length -------> "+result.length)
    if(result.length === 0){
        return result;
    }
    }else{
    // check update data
        findData = {
            _id:data._id,
            review_goals_id:data.review_goals_id,
            year:data.year,
            term:data.term
        }
    result = await ReviewOfGoalsTargetModel.find(findData)   
    if(result.length > 0){
        return result;
    }
    }   
    return DUPLICATE_CODE;
}