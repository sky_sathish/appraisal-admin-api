var ReviewOfGoalsModel = require('../models/review_of_goal.model')
var ReviewOfGoalsTargetModel = require('../models/review_of_goal_target.model')
var DUPLICATE_CODE = 'duplicate';
_this = this
// Async function to get the To do List
exports.getAllReviewOfGoals = async function (callback) {

    // Try Catch the awaited promise to handle the error 
    try {
        var result = await ReviewOfGoalsModel.find()
            .populate("role").populate('category').populate('subcategory');
        // Return the reviewOfGoals list that was retured by the mongoose promise
        if (result != undefined && result.length > 0) {
            return result;
        } else {
            return null;
        }
    } catch (e) {
        // return a Error message describing the reason 
        throw Error('Error while getting ReviewOfGoals')
    }
}

exports.getReviewOfGoalsById = async function (id) {
    // Try Catch the awaited promise to handle the error 
    try {
        var reviewOfGoalsResult = await ReviewOfGoalsModel.findById(id)
            .populate("role").populate('category').populate('subcategory');
        var reviewOfGoalsTargetResult = await ReviewOfGoalsTargetModel.find({ review_goals_id: id })

        if (reviewOfGoalsResult != undefined && reviewOfGoalsTargetResult != undefined) {
            var finalData = {
                "reviewOfGoals": reviewOfGoalsResult,
                "reviewOfGoalsTarget": reviewOfGoalsTargetResult
            }
            return finalData;
        } else {
            return null;
        }
        // Return the roled list that was retured by the mongoose promise
    } catch (e) {
        // return a Error message describing the reason 
        throw Error('Error while getting ReviewOfGoals')
    }
}
exports.createReviewOfGoals = async function (data) {
    // split review of goals data and review of goals target data
    var reviewOfGoalsReqData = data.reviewOfGoals;
    var reviewOfGoalsTargetReqData = data.reviewOfGoalsTarget;
    // check duplicate entry
    var status = await checkDuplicateEntry(reviewOfGoalsReqData)
    console.log("createReviewOfGoals status ---------------> " + status)
    if (status != DUPLICATE_CODE) {
        // Creating a new Mongoose Object by using the new keyword
        var newReviewOfGoals = new ReviewOfGoalsModel({
            function: reviewOfGoalsReqData.function,
            role: reviewOfGoalsReqData.role,
            category: reviewOfGoalsReqData.category,
            subcategory: reviewOfGoalsReqData.subcategory,
            description: reviewOfGoalsReqData.description
        });
        try {
            // Saving the ReviewOfGoals 
            var savedReviewOfGoalsData = await newReviewOfGoals.save()
            // get saved data is
            var id = savedReviewOfGoalsData._id;
            if (id != undefined) {
                var newReviewOfGoalsTarget = new ReviewOfGoalsTargetModel({
                    review_goals_id: id,
                    target: reviewOfGoalsTargetReqData.target,
                    weightage: reviewOfGoalsTargetReqData.weightage,
                    year: reviewOfGoalsTargetReqData.year,
                    term: reviewOfGoalsTargetReqData.term,
                    active_status: true
                })
                var savedReviewOfGoalsTargetData = await newReviewOfGoalsTarget.save()
                if (savedReviewOfGoalsTargetData != undefined) {
                    return "success"
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } catch (e) {
            // return a Error message describing the reason     
            throw Error("Error while Creating Review Of Goals :" + e.message)
        }
    } else {
        return status;
    }
}

exports.updateReviewOfGoals = async function (data) {
    var id = data._id

    try {
        //Find the old ReviewOfGoalsModel Object by the Id
        var oldData = await ReviewOfGoalsModel.findById(id);
    } catch (e) {
        throw Error("Error occured while Finding the ReviewOfGoals")
    }

    // If no old ReviewOfGoals Object exists return null
    if (!oldData) {
        return null;
    }
    //Edit the ReviewOfGoals Object
    oldData.function = data.function;
    oldData.role = data.role;
    oldData.category = data.category;
    oldData.subcategory = data.subcategory;
    oldData.description = data.description;
    try {
        var updatedData = await oldData.save()
        return updatedData;
    } catch (e) {
        throw Error("And Error occured while updating the ReviewOfGoals:" + e.message);
    }
}

exports.deleteReviewOfGoals = async function (id) {

    // Delete the ReviewOfGoals
    try {
        var delete_target = await ReviewOfGoalsTargetModel.remove({ review_goals_id: id })
        if (delete_target.n === 0) {
            throw Error("ReviewOfGoals Could not be deleted")
        }
        var deleted = await ReviewOfGoalsModel.remove({ _id: id })
        if (deleted.n === 0) {
            throw Error("ReviewOfGoals Could not be deleted")
        }

        return "success"
    } catch (e) {
        throw Error("Error Occured while Deleting the ReviewOfGoals")
    }
}

checkDuplicateEntry = async function (data) {
    var findData = {
        function: data.function,
        role: data.role[0],
        category: data.category,
        subcategory: data.subcategory[0],
        description: data.description
    }
    var result = await ReviewOfGoalsModel.find(findData)
    if (result.length === 0) {
        return result;
    } else {
        return DUPLICATE_CODE;
    }
}
// clone section
exports.checkDuplicateEntryForBulkInsertReviewOfGoals = async function (data) {
    var findData = {
        "year": data.toYear,
        "term": data.toTerm
    }
    var result = await ReviewOfGoalsTargetModel.find(findData)
    if (result.length === 0) {
        return result;
    } else {
        return DUPLICATE_CODE;
    }
}
exports.cloneValidation = async function (data) {

        var findData = {
            "year": data.fromYear,
            "term": data.fromTerm
        }
        var reviewOfGoalsTargetarray = [];
        var getReviewOfGoalsTargetData = await ReviewOfGoalsTargetModel.find(findData);

        getReviewOfGoalsTargetData.forEach(elements => {
            var newReviewOfGoalsTargetModelData = ReviewOfGoalsTargetModel({
                "review_goals_id": elements.review_goals_id,
                "target": elements.target,
                "weightage": elements.weightage,
                "year": data.toYear,
                "term": data.toTerm,
                "active_status": true
            })
            reviewOfGoalsTargetarray.push(newReviewOfGoalsTargetModelData)
        })
     
    return reviewOfGoalsTargetarray;
}

exports.cloneReviewOfGoalsTarget = async function (reviewOfGoalsTargetarray) {
    try {
            var resData = await ReviewOfGoalsTargetModel.create(reviewOfGoalsTargetarray)
            .then((res)=>{
                return "Successfully create a clone - Review of Goals."
            })
            .catch((err)=>{
                console.log(err)
                return "Error on Bulk clone Creation!"
            })
           return resData;
        } catch (e) {
            throw Error("Error while Creating clone for Review of Goals.")
        }
}
// clone section end

