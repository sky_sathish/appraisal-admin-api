var GoalSubCategory = require('../models/goal_subcategory.model')

// Saving the context of this module inside the _the variable
_this = this

// Async function to get the To do List
exports.getGoalSubCategory = async function(query, page, limit){

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var goalSubCategory = await GoalSubCategory.paginate(query, options)
        
        // Return the roled list that was retured by the mongoose promise
        return goalSubCategory;

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating GoalSubCategory')
    }
}
exports.getGoalSubCategoryById = async function(id,query, page, limit){

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var goalSubCategory = await GoalSubCategory.findById(id);
        if(goalSubCategory!=undefined){
            return goalSubCategory;
        }else{
            return "Data not found";
        }
        // Return the roled list that was retured by the mongoose promise
        

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating GoalSubCategory')
    }
}

exports.createGoalSubCategory = async function(goalSubCategory){
    
    // Creating a new Mongoose Object by using the new keyword
    var newGoalSubCategory = new GoalSubCategory({
        name: goalSubCategory.name,
    })

    try{

        // Saving the GoalSubCategory 
        var savedGoalSubCategory = await newGoalSubCategory.save()

        return savedGoalSubCategory;
    }catch(e){
      
        // return a Error message describing the reason     
        throw Error("Error while Creating GoalSubCategory:"+e.message);
    }
}

exports.updateGoalSubCategory = async function(goalSubCategory){
    var id = goalSubCategory.id

    try{
        //Find the old GoalSubCategory Object by the Id
    
        var oldGoalSubCategory = await GoalSubCategory.findById(id);
    }catch(e){
        throw Error("Error occured while Finding the GoalSubCategory")
    }

    // If no old GoalSubCategory Object exists return false
    if(!oldGoalSubCategory){
        return false;
    }

    console.log(oldGoalSubCategory)

    //Edit the GoalSubCategory Object
    oldGoalSubCategory.name = goalSubCategory.name


    console.log(oldGoalSubCategory)

    try{
        var savedGoalSubCategory = await oldGoalSubCategory.save()
        return savedGoalSubCategory;
    }catch(e){
        throw Error("And Error occured while updating the GoalSubCategory:"+e.message);
    }
}

exports.deleteGoalSubCategory = async function(id){
    
    // Delete the GoalSubCategory
    try{
        var deleted = await GoalSubCategory.remove({_id: id})
        if(deleted.n === 0){
            throw Error("GoalSubCategory Could not be deleted")
        }
        return deleted
    }catch(e){
        throw Error("Error Occured while Deleting the GoalSubCategory")
    }
}