var AppraisalMaster = require('../models/appraisal.master.model.js');
var AppraisalUser = require('../models/appraisal.user.model.js');
var dateFormat = require('dateformat');
var requestApi = require('sync-request');

_this = this

exports.getAppraisalMasters = async function (query, page, limit) {
    var options = {
        page,
        limit
    }
    try {
        var appraisalMasters = await AppraisalMaster.find({});
        return appraisalMasters;
    } catch (e) {
        throw Error('Error while Paginating AppraisalMasters')
    }
}

exports.getAppraisalMasterById = async function (id, query, page, limit) {
    var options = {
        page,
        limit
    }
    try {
        var appraisalMaster = await AppraisalMaster.findById(id);
        if (appraisalMaster != undefined) {
            return appraisalMaster;
        } else {
            return "Data not found";
        }
    } catch (e) {
        throw Error('Error while Paginating AppraisalMaster')
    }
}

exports.createAppraisalMaster = async function (appraisalMaster) {
    var newAppraisalMaster = new AppraisalMaster({
        appraisal_name: appraisalMaster.appraisal_name,
        appraisal_intro: appraisalMaster.appraisal_intro,
        type: appraisalMaster.type,
        due_date: appraisalMaster.due_date,
        status: appraisalMaster.status,
        created_date: dateFormat(new Date(appraisalMaster.created_date), "yyyy-mm-dd"),
        modified_date: dateFormat(new Date(appraisalMaster.modified_date), "yyyy-mm-dd"),
        created_by: appraisalMaster.created_by,
        modified_by: appraisalMaster.modified_by,
        islocked: false
    });

    console.log("create date : " + newAppraisalMaster.created_date)
    try {
        console.log("---------------------------------------");
        var savedAppraisalMaster = await newAppraisalMaster.save();
        return savedAppraisalMaster;
    } catch (e) {
        throw Error("Error while Creating AppraisalMaster" + e.message)
    }
}


// exports.createAppraisalMaster = async function (appraisalMaster) {
//     var newAppraisalMaster = new AppraisalMaster({
//         appraisal_name: appraisalMaster.appraisal_name,
//         type: appraisalMaster.type,
//         due_date: appraisalMaster.due_date,
//         status: appraisalMaster.status,
//         created_date: dateFormat(new Date(appraisalMaster.created_date), "yyyy-mm-dd"),
//         modified_date: dateFormat(new Date(appraisalMaster.modified_date), "yyyy-mm-dd"),
//         created_by: appraisalMaster.created_by,
//         modified_by: appraisalMaster.modified_by
//     });

//     console.log("create date : " + newAppraisalMaster.created_date)
//     try {
//         console.log("---------------------------------------");
//         var savedAppraisalMaster = await newAppraisalMaster.save();
//         return savedAppraisalMaster;
//     } catch (e) {
//         throw Error("Error while Creating AppraisalMaster" + e.message)
//     }
// }

exports.updateAppraisalMaster = async function (appraisalMaster) {
    var id = appraisalMaster.id
    try {
        var oldAppraisalMaster = await AppraisalMaster.findById(id);
    } catch (e) {
        throw Error("Error occured while Finding the AppraisalMaster")
    }
    if (!oldAppraisalMaster) {
        return false;
    }
    // console.log(oldAppraisalMaster)
    oldAppraisalMaster.appraisal_name = appraisalMaster.appraisal_name;
    oldAppraisalMaster.appraisal_intro = appraisalMaster.appraisal_intro;
    oldAppraisalMaster.type = appraisalMaster.type;
    oldAppraisalMaster.due_date = appraisalMaster.due_date;
    oldAppraisalMaster.status = appraisalMaster.status;
    oldAppraisalMaster.created_date = appraisalMaster.created_date;
    oldAppraisalMaster.modified_date = appraisalMaster.modified_date;
    oldAppraisalMaster.created_by = appraisalMaster.created_by;
    oldAppraisalMaster.modified_by = appraisalMaster.modified_by;
    oldAppraisalMaster.islocked = appraisalMaster.islocked;

    try {
        var savedAppraisalMaster = await oldAppraisalMaster.save();
        return savedAppraisalMaster;
    } catch (e) {
        throw Error("And Error occured while updating the Appraisal" + e.message);
    }
}


// exports.updateAppraisalMasterLockStatus = async function (appraisalMaster) {
//     var id = appraisalMaster.id
//     try {
//         var oldAppraisalMaster = await AppraisalMaster.findById(id);
//     } catch (e) {
//         throw Error("Error occured while Finding the AppraisalMaster")
//     }
//     if (!oldAppraisalMaster) {
//         return false;
//     }
//     // console.log(oldAppraisalMaster)
//     oldAppraisalMaster.appraisal_name = appraisalMaster.appraisal_name;
//     oldAppraisalMaster.type = appraisalMaster.type;
//     oldAppraisalMaster.due_date = appraisalMaster.due_date;
//     oldAppraisalMaster.status = appraisalMaster.status;
//     oldAppraisalMaster.created_date = appraisalMaster.created_date;
//     oldAppraisalMaster.modified_date = appraisalMaster.modified_date;
//     oldAppraisalMaster.created_by = appraisalMaster.created_by;
//     oldAppraisalMaster.modified_by = appraisalMaster.modified_by;
//     oldAppraisalMaster.islocked = false;

//     try {
//         var savedAppraisalMaster = await oldAppraisalMaster.save();
//         return savedAppraisalMaster;
//     } catch (e) {
//         throw Error("And Error occured while updating the Appraisal" + e.message);
//     }
// }


exports.deleteAppraisalMaster = async function (appraisal_id) {
    try {
        var deletedUsers = await AppraisalUser.remove({
            appraisal_id: { '$in': appraisal_id }
        })
        var deleted = await AppraisalMaster.remove({
            _id: appraisal_id
        })

        if (deleted.n === 0 && deletedUsers.n === 0) {
            throw Error("AppraisalMaster Could not be deleted")
        }

        var employeeAppraisalAPI = "http://localhost:3002/api/employee-appraisal?appraisal_id="
        try {
            var res = requestApi('DELETE', employeeAppraisalAPI + appraisal_id);
            var user = JSON.parse(res.getBody('utf8'));
            console.log("---------------Bulk Appraisal_id data delete Result---------------")
            return "success";
        } catch (e) {
            console.log(e)
        }


        return deleted;
    } catch (e) {
        throw Error("Error Occured while Deleting the AppraisalMaster")
    }
}

