var Role = require('../models/role.model')

// Saving the context of this module inside the _the variable
_this = this

// Async function to get the To do List
exports.getRoles = async function (query, page, limit) {
   
    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }

    // Try Catch the awaited promise to handle the error 
    
    try {
        var roles = await Role.paginate(query, options);
      
        return roles;

    } catch (e) {
      
        // return a Error message describing the reason 
        throw Error('Error while Paginating Roles')
    }
}

// Async function to get the To do List
exports.getAllRoles = async function (callback) {

    
    // Try Catch the awaited promise to handle the error 

    try {
        var roles = await Role.find();
        
        // Return the roled list that was retured by the mongoose promise

        return roles;

    } catch (e) {
       
        // return a Error message describing the reason 
        throw Error('Error while Paginating Roles')
    }
}

exports.getRolesById = async function (id, query, page, limit) {

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }

    // Try Catch the awaited promise to handle the error 

    try {
        var roles = await Role.findById(id);
        if (roles != undefined) {
            return roles;
        } else {
            return "Data not found";
        }
        // Return the roled list that was retured by the mongoose promise


    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating Roles')
    }
}
exports.getRolesByName = async function (name, query, page, limit) {

   
    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }

    // Try Catch the awaited promise to handle the error 

    try {
        var roles = await Role.find({"name":name});
      
        if (roles != undefined) {
            return roles;
        } else {
            return "Data not found";
        }
        // Return the roled list that was retured by the mongoose promise


    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating Roles')
    }
}


exports.createRole = async function (role) {

    // Creating a new Mongoose Object by using the new keyword
    var newRole = new Role({
        name: role.name
    });

    try {

        // Saving the Role 
        var savedRole = await newRole.save()

        return savedRole;
    } catch (e) {

        // return a Error message describing the reason     
        throw Error("Error while Creating Role :"+e.message)
    }
}



exports.createRoleBulk = async function (roleArr) {

    try {
        
        Role.create(roleArr,function (err, docs) {
            if (err){ 
                return console.error(err);
            } else {
              console.log("Multiple documents inserted to Role");
              return "Multiple documents inserted to Role";
            }});
      
    } catch (e) {
        throw Error("Error while Creating Role")
    }
}


exports.updateRole = async function (role) {
    var id = role.id

    try {
        //Find the old Role Object by the Id

        var oldRole = await Role.findById(id);
    } catch (e) {
        throw Error("Error occured while Finding the Role")
    }

    // If no old Role Object exists return false
    if (!oldRole) {
        return false;
    }

    console.log(oldRole)

    //Edit the Role Object
    oldRole.name = role.name


    console.log(oldRole)

    try {
        var savedRole = await oldRole.save()
        return savedRole;
    } catch (e) {
        throw Error("And Error occured while updating the Role:"+e.message);
    }
}

exports.deleteRole = async function (id) {

    // Delete the Role
    try {
        var deleted = await Role.remove({ _id: id })
        if (deleted.n === 0) {
            throw Error("Role Could not be deleted")
        }
        var data = await Role.find({});
        return data
    } catch (e) {
        throw Error("Error Occured while Deleting the Role")
    }
}