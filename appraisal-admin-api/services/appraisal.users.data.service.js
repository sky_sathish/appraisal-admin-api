var requestApi = require('sync-request');
var jsonfile = require('jsonfile');
var ApprisalUser = require('../models/appraisal.user.model')
var parseString = require('xml2js').parseString;
var username = "rage";
var password = "rg2011";
var auth = "Basic " + new Buffer(username + ":" + password).toString("base64");

exports.getAppraisalUsers = async (callback) => {
    var userApiURL = 'http://emp.intranet.ragecom.com/service/user/infov1';
    try {
        var res = requestApi('GET', userApiURL, {
            headers: {
                "Authorization": auth
            }
        });
        await parseString(res.getBody('utf8'), callback);
    } catch (err) {
        throw Error("Exception caught");
    }
}

exports.addEmployeeAppraisal = async (sendData) => {
    var employeeAppraisalAPI = "http://localhost:3002/api/employee-appraisal?bk"
    try {
        var res = requestApi('POST', employeeAppraisalAPI, {
            json: sendData
        });
        var user = JSON.parse(res.getBody('utf8'));
        return "success";
    } catch (e) {
        return "error " + e;
    }
}


exports.updateEmployeeAppraisaldetails = async (sendData) => {
    var employeeAppraisalAPI = "http://localhost:3002/api/employee-appraisal?emp"
    try {
        var res = requestApi('PUT', employeeAppraisalAPI, {
            json: sendData
        });
        var user = JSON.parse(res.getBody('utf8'));
        return user;
    } catch (e) {
        return "error " + e;
    }
}

