var GeneralConfig = require('../models/general_config.model')

// Saving the context of this module inside the _the variable
_this = this

// Async function to get the To do List
exports.getGeneralConfigs = async function(query, page, limit){

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var generalConfigs = await GeneralConfig.paginate(query, options)
        
        // Return the generalConfigd list that was retured by the mongoose promise
        return generalConfigs;

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating GeneralConfigs')
    }
}
exports.getGeneralConfigsById = async function(id,query, page, limit){

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var generalConfigs = await GeneralConfig.findById(id);
        if(generalConfigs!=undefined){
            return generalConfigs;
        }else{
            return "Data not found";
        }
        // Return the generalConfigd list that was retured by the mongoose promise
       

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating GeneralConfigs')
    }
}


exports.createGeneralConfig = async function(generalConfig){
    
    // Creating a new Mongoose Object by using the new keyword
    var newGeneralConfig = new GeneralConfig({
        key: generalConfig.key,
        key_desc: generalConfig.key_desc,
        value: generalConfig.value,
        tag: generalConfig.tag,
        sequence: generalConfig.sequence,
        description:generalConfig.description
    })

    try{

        // Saving the GeneralConfig 
        var savedGeneralConfig = await newGeneralConfig.save()

        return savedGeneralConfig;
    }catch(e){
      
        // return a Error message describing the reason     
        throw Error("Error while Creating GeneralConfig:"+e.message);
    }
}

exports.updateGeneralConfig = async function(generalConfig){
    var id = generalConfig.id

    try{
        //Find the old GeneralConfig Object by the Id
    
        var oldGeneralConfig = await GeneralConfig.findById(id);
    }catch(e){
        throw Error("Error occured while Finding the GeneralConfig")
    }

    // If no old GeneralConfig Object exists return false
    if(!oldGeneralConfig){
        return false;
    }

    console.log(oldGeneralConfig)

    //Edit the GeneralConfig Object
    oldGeneralConfig.key = generalConfig.key;
    oldGeneralConfig.key_desc = generalConfig.key_desc;
    oldGeneralConfig.value = generalConfig.value;
    oldGeneralConfig.tag = generalConfig.tag;
    oldGeneralConfig.sequence = generalConfig.sequence;
    oldGeneralConfig.description=generalConfig.description;

    console.log(oldGeneralConfig)

    try{
        var savedGeneralConfig = await oldGeneralConfig.save()
        return savedGeneralConfig;
    }catch(e){
        throw Error("And Error occured while updating the GeneralConfig:"+e.message);
    }
}

exports.deleteGeneralConfig = async function(id){
    
    // Delete the GeneralConfig
    try{
        var deleted = await GeneralConfig.remove({_id: id})
        if(deleted.n === 0){
            throw Error("GeneralConfig Could not be deleted")
        }
        
        console.log("deleted")
        console.log(deleted)
        return deleted
    }catch(e){
        console.log("Error")
        console.log(e)
        throw Error("Error Occured while Deleting the GeneralConfig"+e)
    }
}