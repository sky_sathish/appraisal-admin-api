var CompetenciesAttribute = require('../models/competencies_attribute.model')

// Saving the context of this module inside the _the variable
_this = this

// Async function to get the To do List
exports.getCompetenciesAttributes = async function(query, page, limit){

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var competenciesAttributes = await CompetenciesAttribute.paginate(query, options)
        
        // Return the competenciesAttributed list that was retured by the mongoose promise
        return competenciesAttributes;

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating CompetenciesAttributes')
    }
}
exports.getCompetenciesAttributesById = async function(id,query, page, limit){

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var competenciesAttributes = await CompetenciesAttribute.findById(id);
        if(competenciesAttributes!=undefined){
            return competenciesAttributes;
        }else{
            return "Data not found";
        }
        // Return the competenciesAttributed list that was retured by the mongoose promise
        

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating CompetenciesAttributes')
    }
}
exports.createCompetenciesAttribute = async function(competenciesAttribute){
    
    // Creating a new Mongoose Object by using the new keyword
    var newCompetenciesAttribute = new CompetenciesAttribute({
        attribute:competenciesAttribute.attribute,
        attribute_description:competenciesAttribute.attribute_description
    })

    try{

        // Saving the CompetenciesAttribute 
        var savedCompetenciesAttribute = await newCompetenciesAttribute.save()

        return savedCompetenciesAttribute;
    }catch(e){
      
        // return a Error message describing the reason     
        throw Error("Error while Creating CompetenciesAttribute:"+e.message);
    }
}

exports.updateCompetenciesAttribute = async function(competenciesAttribute){
    var id = competenciesAttribute.id

    try{
        //Find the old CompetenciesAttribute Object by the Id
    
        var oldCompetenciesAttribute = await CompetenciesAttribute.findById(id);
    }catch(e){
        throw Error("Error occured while Finding the CompetenciesAttribute")
    }

    // If no old CompetenciesAttribute Object exists return false
    if(!oldCompetenciesAttribute){
        return false;
    }

    console.log(oldCompetenciesAttribute)

    //Edit the CompetenciesAttribute Object
    // oldCompetenciesAttribute.name = competenciesAttribute.name
    oldCompetenciesAttribute.attribute = competenciesAttribute.attribute;
    oldCompetenciesAttribute.attribute_description = competenciesAttribute.attribute_description;

    console.log(oldCompetenciesAttribute)

    try{
        var savedCompetenciesAttribute = await oldCompetenciesAttribute.save()
        return savedCompetenciesAttribute;
    }catch(e){
        throw Error("And Error occured while updating the CompetenciesAttribute:"+e.message);
    }
}

exports.deleteCompetenciesAttribute = async function(id){
    
    // Delete the CompetenciesAttribute
    try{
        var deleted = await CompetenciesAttribute.remove({_id: id})
        if(deleted.n === 0){
            throw Error("CompetenciesAttribute Could not be deleted")
        }
        return deleted
    }catch(e){
        throw Error("Error Occured while Deleting the CompetenciesAttribute")
    }
}