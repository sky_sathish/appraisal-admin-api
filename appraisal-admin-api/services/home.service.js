var GoalAttributeService = require('../services/goal_attribute.service')
var RoleService = require('../services/role.service')
var GoalCategoryService = require('../services/goal_category.service')
var GoalSubcategoryService = require('../services/goal_subcategory.service')
var MatricLookForLastYearService = require('../services/matric_look_for_last_year.service')
var CompetenciesAttributeService = require('../services/competencies_attributes.service')
var CompetenciesLevelService = require('../services/competencies_level.service')


// Saving the context of this module inside the _the variable
_this = this

// Async function to get the To do List
exports.getHome = async function(query, page, limit){
    var res = {} 
    res["role"]=await RoleService.getRoles({}, page, limit)
    res["goalCategory"]=await GoalCategoryService.getGoalCategorys({}, page, limit)
    res["goalSubcategory"]=await GoalSubcategoryService.getGoalSubCategory({}, page, limit)
    res["matrixLookForLastYear"]=await MatricLookForLastYearService.getMatrixsLookForLastYear({}, page, limit)
    res["goalAttribute"]=await GoalAttributeService.getGoalAttributes({}, page, limit);
    res["competenciesAttribute"]=await CompetenciesAttributeService.getCompetenciesAttributes({}, page, limit);
    res["competenciesLevel"]=await CompetenciesLevelService.getCompetenciesLevel({}, page, limit);

    return res;
}

