var MatrixLookForLastYear = require('../models/matric_look_for_last_year.model')

// Saving the context of this module inside the _the variable
_this = this

// Async function to get the To do List
exports.getMatrixsLookForLastYear = async function (query, page, limit) {

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }

    // Try Catch the awaited promise to handle the error 

    try {
        var matrixsLookForLastYear = await MatrixLookForLastYear.paginate(query, options)

        // Return the MatrixLookForLastYeard list that was retured by the mongoose promise
        return matrixsLookForLastYear;

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating matrixsLookForLastYear')
    }
}

exports.getMatrixsLookForLastYearById = async function (id, query, page, limit) {

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }

    // Try Catch the awaited promise to handle the error 

    try {
        var matrixsLookForLastYear = await MatrixLookForLastYear.findById(id);
        if(matrixsLookForLastYear!=undefined){
            return matrixsLookForLastYear;
        }else{
            return "Data not found";
        }

    } catch (e) {
        console.log(e);
        // return a Error message describing the reason 
        throw Error('Error while Paginating matrixsLookForLastYear')
    }
}

exports.createMatrixLookForLastYear = async function (matrixLookForLastYear) {

    // Creating a new Mongoose Object by using the new keyword
    var newMatrixLookForLastYear = new MatrixLookForLastYear({
        name: matrixLookForLastYear.name
    })
    try {

        // Saving the MatrixLookForLastYear 
        var savedMatrixLookForLastYear = await newMatrixLookForLastYear.save()

        return savedMatrixLookForLastYear;
    } catch (e) {

        // return a Error message describing the reason     
        throw Error("Error while Creating MatrixLookForLastYear:"+e.message);
    }
}

exports.updateMatrixLookForLastYear = async function (matrixLookForLastYear) {
    var id = matrixLookForLastYear.id

    try {
        //Find the old MatrixLookForLastYear Object by the Id

        var oldMatrixLookForLastYear = await MatrixLookForLastYear.findById(id);
    } catch (e) {
        throw Error("Error occured while Finding the MatrixLookForLastYear")
    }

    // If no old MatrixLookForLastYear Object exists return false
    if (!oldMatrixLookForLastYear) {
        return false;
    }

    console.log(oldMatrixLookForLastYear)

    //Edit the MatrixLookForLastYear Object
    oldMatrixLookForLastYear.name = matrixLookForLastYear.name

    console.log(oldMatrixLookForLastYear)

    try {
        var savedMatrixLookForLastYear = await oldMatrixLookForLastYear.save()
        return savedMatrixLookForLastYear;
    } catch (e) {
        throw Error("And Error occured while updating the MatrixLookForLastYear:"+e.message);
    }
}

exports.deleteMatrixLookForLastYear = async function (id) {

    // Delete the MatrixLookForLastYear
    try {
        var deleted = await MatrixLookForLastYear.remove({ _id: id })
        if (deleted.n === 0) {
            throw Error("MatrixLookForLastYear Could not be deleted")
        }
        return deleted
    } catch (e) {
        throw Error("Error Occured while Deleting the MatrixLookForLastYear")
    }
}