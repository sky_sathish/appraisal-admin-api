var GoalCategory = require('../models/goal_category.model')

// Saving the context of this module inside the _the variable
_this = this

// Async function to get the To do List
exports.getGoalCategorys = async function(query, page, limit){

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var goalCategorys = await GoalCategory.paginate(query, options)
        
        // Return the goalCategoryd list that was retured by the mongoose promise
        return goalCategorys;

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating GoalCategorys')
    }
}
exports.getGoalCategorysById = async function(id,query, page, limit){

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var goalCategorys = await GoalCategory.findById(id);
        if(goalCategorys!=undefined){
            return goalCategorys;
        }else{
            return "Data not found";
        }
        // Return the goalCategoryd list that was retured by the mongoose promise
       

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating GoalCategorys')
    }
}
exports.createGoalCategory = async function(goalCategory){
    
    // Creating a new Mongoose Object by using the new keyword
    var newGoalCategory = new GoalCategory({
        name: goalCategory.name
    })

    try{

        // Saving the GoalCategory 
        var savedGoalCategory = await newGoalCategory.save()

        return savedGoalCategory;
    }catch(e){
      
        // return a Error message describing the reason     
        throw Error("Error while Creating GoalCategory:"+e.message);
    }
}

exports.updateGoalCategory = async function(goalCategory){
    var id = goalCategory.id

    try{
        //Find the old GoalCategory Object by the Id
    
        var oldGoalCategory = await GoalCategory.findById(id);
    }catch(e){
        throw Error("Error occured while Finding the GoalCategory")
    }

    // If no old GoalCategory Object exists return false
    if(!oldGoalCategory){
        return false;
    }

    console.log(oldGoalCategory)

    //Edit the GoalCategory Object
    oldGoalCategory.name = goalCategory.name


    console.log(oldGoalCategory)

    try{
        var savedGoalCategory = await oldGoalCategory.save()
        return savedGoalCategory;
    }catch(e){
        throw Error("And Error occured while updating the GoalCategory:"+e.message);
    }
}

exports.deleteGoalCategory = async function(id){
    
    // Delete the GoalCategory
    try{
        var deleted = await GoalCategory.remove({_id: id})
        if(deleted.n === 0){
            throw Error("GoalCategory Could not be deleted")
        }
        return deleted
    }catch(e){
        throw Error("Error Occured while Deleting the GoalCategory")
    }
}