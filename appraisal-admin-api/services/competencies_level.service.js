var CompetenciesLevel = require('../models/competencies_level.model')

// Saving the context of this module inside the _the variable
_this = this

// Async function to get the To do List
exports.getCompetenciesLevel = async function (query, page, limit) {
    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    // Try Catch the awaited promise to handle the error 
    try {
        var competenciesLevel = await CompetenciesLevel.paginate(query, options)
        if (competenciesLevel != undefined) {
            return competenciesLevel;
        } else {
            return "Data not found";
        }
        // Return the roled list that was retured by the mongoose promise
        return competenciesLevel;
    } catch (e) {
        // return a Error message describing the reason 
        throw Error('Error while Paginating competencies Level')
    }
}
exports.getCompetenciesLevelById = async function (id, query, page, limit) {

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }

    // Try Catch the awaited promise to handle the error 

    try {
        var competenciesLevel = await CompetenciesLevel.findById(id);
        if (competenciesLevel != undefined) {
            return competenciesLevel;
        } else {
            return "Data not found";
        }
        // Return the roled list that was retured by the mongoose promise


    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating Competencies Level')
    }
}
exports.createCompetenciesLevel = async function (competenciesLevel) {

    // Creating a new Mongoose Object by using the new keyword
    var newCompetenciesLevel = new CompetenciesLevel({
        level_name: competenciesLevel.level_name,
        level_desc: competenciesLevel.level_desc,
        sequence: competenciesLevel.sequence,
        weightage: competenciesLevel.weightage,
        rating_scale: competenciesLevel.rating_scale,
        scenario_1: competenciesLevel.scenario_1,
        scenario_2: competenciesLevel.scenario_2
    });

    try {

        // Saving the CompetenciesLeve 
        var savedCompetenciesLevel = await newCompetenciesLevel.save()

        return savedCompetenciesLevel;
    } catch (e) {

        // return a Error message describing the reason     
        throw Error("Error while Creating Competencies Level:"+e.message);
    }
}

exports.updateCompetenciesLevel = async function (competenciesLevel) {
    var id = competenciesLevel.id

    try {
        //Find the old competenciesLevel Object by the Id

        var oldCompetenciesLevel = await CompetenciesLevel.findById(id);
    } catch (e) {
        throw Error("Error occured while Finding the Role")
    }

    // If no old Role Object exists return false
    if (!oldCompetenciesLevel) {
        return false;
    }

    console.log(oldCompetenciesLevel)

    //Edit the Role Object
    oldCompetenciesLevel.level_name = competenciesLevel.level_name,
    oldCompetenciesLevel.level_desc = competenciesLevel.level_desc,
    oldCompetenciesLevel.sequence = competenciesLevel.sequence,
    oldCompetenciesLevel.weightage = competenciesLevel.weightage,
    oldCompetenciesLevel.rating_scale = competenciesLevel.rating_scale,
    oldCompetenciesLevel.scenario_1 = competenciesLevel.scenario_1,
    oldCompetenciesLevel.scenario_2 = competenciesLevel.scenario_2


    console.log(oldCompetenciesLevel)

    try {
        var savedCompetenciesLevel = await oldCompetenciesLevel.save()
        return savedCompetenciesLevel;
    } catch (e) {
        throw Error("And Error occured while updating the CompetenciesLevel:"+e.message);
    }
}

exports.deleteCompetenciesLevel = async function (id) {

    // Delete the CompetenciesLevel
    try {
        console.log("id -> "+id)
        var deleted = await CompetenciesLevel.remove({ _id: id })
        console.log(deleted)
        if (deleted.n === 0) {
            throw Error("Role Could not be deleted")
        }
        return deleted
    } catch (e) {
        throw Error("Error Occured while Deleting the CompetenciesLevel" + e)
    }
}