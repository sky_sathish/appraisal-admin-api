var ReviewOfCompetenciesLevelModel = require('../models/review_of_competencies_level.model')

var SAVE_ENTRY_CODE = 'save';
var UPDATE_ENTRY_CODE = 'update';
var DUPLICATE_CODE= 'duplicate';

exports.createReviewOfCompetenciesLevel = async function (data) {

    var status = await checkDuplicateEntryForCompetenciesLevel(data,SAVE_ENTRY_CODE)
    console.log("createReviewOfCompetenciesLevel status -------> "+status)
    if( status != DUPLICATE_CODE){
    // Creating a new Mongoose Object by using the new keyword
    var newData = new ReviewOfCompetenciesLevelModel({
        review_competencies_id: data.review_competencies_id,
        competencies_level: data.competencies_level,
        weightage: data.weightage,
        year: data.year,
        term: data.term,
        active_status: true
    });
    try {
        // Saving the ReviewOfCompetenciesLevel
        var savedData = await newData.save()
        return savedData;
    } catch (e) {
        // return a Error message describing the reason     
        throw Error("Error while Creating Review Of Competencies Level :"+e.message)
    }
}else{
    return status;
}
}
exports.updateReviewOfCompetenciesLevel = async function (data) {
  
    var status = await checkDuplicateEntryForCompetenciesLevel(data,UPDATE_ENTRY_CODE)
    console.log("updateReviewOfCompetenciesLevel status -------> "+status)
    if( status != DUPLICATE_CODE){
    var id = data._id
    try {
        //Find the old ReviewOfCompetenciesLevelModel Object by the Id
        var oldData = await ReviewOfCompetenciesLevelModel.findById(id);
    } catch (e) {
        throw Error("Error occured while Finding the ReviewOfCompetenciesLevel")
    }
    // If no old ReviewOfCompetenciesLevel Object exists return false
    if (!oldData) {
        return false;
    }
    //Edit the ReviewOfCompetenciesLevel Object
    oldData.review_competencies_id = data.review_competencies_id;
    oldData.competencies_level = data.competencies_level;
    oldData.weightage = data.weightage;
    oldData.year = data.year;
    oldData.term = data.term;
    oldData.active_status = data.active_status;
    try {
        var updatedData = await oldData.save()
        return updatedData;
    } catch (e) {
        throw Error("And Error occured while updating the ReviewOfCompetenciesLevel:"+e.message);
    }
    }else{
    return status;
    }
}

exports.deleteReviewOfCompetenciesLevel = async function (id) {

    // Delete the ReviewOfCompetenciesLevel
    try {
        var deleted = await ReviewOfCompetenciesLevelModel.remove({ _id: id })
        if (deleted.n === 0) {
            throw Error("ReviewOfCompetenciesLevel Could not be deleted")
        }
        return "success"
    } catch (e) {
        throw Error("Error Occured while Deleting the ReviewOfCompetenciesLevel")
    }
}

checkDuplicateEntryForCompetenciesLevel = async function(data,action){
    var findData;
    var result;
    // check save data
    if(action == SAVE_ENTRY_CODE){
    findData = {
        review_competencies_id:data.review_competencies_id,
        year:data.year,
        term:data.term
    }
    result = await ReviewOfCompetenciesLevelModel.find(findData)
    console.log("result.length -------> "+result.length)
    if(result.length === 0){
        return result;
    }
    }else{
    // check update data
        findData = {
            _id:data._id,
            review_competencies_id:data.review_competencies_id,
            year:data.year,
            term:data.term
        }
    result = await ReviewOfCompetenciesLevelModel.find(findData)   
    if(result.length > 0){
        return result;
    }
    }   
    return DUPLICATE_CODE;
}