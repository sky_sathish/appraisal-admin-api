var ReviewOfCompetenciesModel = require('../models/review_of_competencies.model')
var ReviewOfCompetenciesLevelModel = require('../models/review_of_competencies_level.model')
var DUPLICATE_CODE= 'duplicate';
_this = this
// Async function to get the To do List
exports.getAllReviewOfCompetencies = async function (callback) {

    // Try Catch the awaited promise to handle the error 
    try {
        var result = await ReviewOfCompetenciesModel.find()
        .populate("role");
        // Return the getAllReviewOfCompetencies list that was retured by the mongoose promise
        if(result !=undefined && result.length >0){
        return result;
        }else{
            return null;
        }
    } catch (e) {
        // return a Error message describing the reason 
        throw Error('Error while getting getAllReviewOfCompetencies')
    }
}

exports.getReviewOfCompetenciesById = async function (id) {
    // Try Catch the awaited promise to handle the error 
    try {
        var reviewOfCompetenciesResult = await ReviewOfCompetenciesModel.findById(id)
        .populate("role");
        var reviewOfCompetenciesLevelResult = await ReviewOfCompetenciesLevelModel.find({review_competencies_id:id})
        .populate("competencies_level")

        if (reviewOfCompetenciesResult != undefined && reviewOfCompetenciesLevelResult != undefined) {
            var finalData = {
                "reviewOfCompetencies" :reviewOfCompetenciesResult,
                "reviewOfCompetenciesLevel" :reviewOfCompetenciesLevelResult
            }
            return finalData;
        } else {
            return null;
        }
        // Return the roled list that was retured by the mongoose promise
    } catch (e) {
        // return a Error message describing the reason 
        throw Error('Error while getting reviewOfCompetencies')
    }
}
exports.createReviewOfCompetencies = async function (data) {
    // split review of goals data and review of goals target data
    var reviewOfCompetenciesReqData = data.reviewOfCompetencies;
    var reviewOfCompetenciesLevelReqData = data.reviewOfCompetenciesLevel;
    // check duplicate entry
    var status = await checkDuplicateEntryForCompetencies(reviewOfCompetenciesReqData)
   console.log("createReviewOfCompetencies status ---------------> "+status)
    if(status != DUPLICATE_CODE){
    // Creating a new Mongoose Object by using the new keyword
    var newReviewOfCompetencies = new ReviewOfCompetenciesModel({
        function: reviewOfCompetenciesReqData.function,
        role: reviewOfCompetenciesReqData.role,
        description: reviewOfCompetenciesReqData.description
    });
    try {
        // Saving the ReviewOfGoals 
        var savedReviewOfCompetenciesData = await newReviewOfCompetencies.save()
        // get saved data is
        var id = savedReviewOfCompetenciesData._id;
        if(id != undefined){
        var newReviewOfGCompetenciesLevel = new ReviewOfCompetenciesLevelModel({
            review_competencies_id: id,
            competencies_level: reviewOfCompetenciesLevelReqData.competencies_level,
            weightage: reviewOfCompetenciesLevelReqData.weightage,
            year: reviewOfCompetenciesLevelReqData.year,
            term: reviewOfCompetenciesLevelReqData.term,
            active_status: true
        })
        var savedReviewOfCompetenciesLevelData = await newReviewOfGCompetenciesLevel.save()
        if(savedReviewOfCompetenciesLevelData != undefined){
            return "success"
        }else{
            return null;
        }
        }else{
            return null;
        }
    } catch (e) {
        // return a Error message describing the reason     
        throw Error("Error while Creating Review Of Competencies :"+e.message)
    }
}else{
    return status;
}
}

exports.updateReviewOfCompetencies = async function (data) {
    var id = data._id

    try {
        //Find the old ReviewOfCompetenciesModel Object by the Id
        var oldData = await ReviewOfCompetenciesModel.findById(id);
    } catch (e) {
        throw Error("Error occured while Finding the ReviewOfCompetencies")
    }

    // If no old ReviewOfCompetencies Object exists return null
    if (!oldData) {
        return null;
    }
    //Edit the ReviewOfCompetencies Object
    oldData.function = data.function;
    oldData.role = data.role;
    oldData.description = data.description;
    try {
        var updatedData = await oldData.save()
        return updatedData;
    } catch (e) {
    oldData.subcategory = data.subcategory;
        throw Error("And Error occured while updating the ReviewOfCompetencies:"+e.message);
    }
}

exports.deleteReviewOfCompetencies = async function (id) {

    // Delete the ReviewOfCompetencies
    try {
        var delete_level = await ReviewOfCompetenciesLevelModel.remove({ review_competencies_id: id })
        if (delete_level.n === 0) {
            throw Error("ReviewOfCompetenciesLevelModel Could not be deleted")
        }
        var deleted = await ReviewOfCompetenciesModel.remove({ _id: id })
        if (deleted.n === 0) {
            throw Error("ReviewOfCompetenciesModel Could not be deleted")
        }
        
        return "success"
    } catch (e) {
        throw Error("Error Occured while Deleting the ReviewOfCompetenciesModel")
    }
}

checkDuplicateEntryForCompetencies = async function (data){
    var findData = {
        function : data.function,
        role :  data.role[0],
        description :  data.description
    }
    var result = await ReviewOfCompetenciesModel.find(findData)
    if(result.length === 0){
        return result;
    }else{
    return DUPLICATE_CODE;
    }
}

// clone section
exports.checkDuplicateEntryForBulkInsertReviewOfCompetencies = async function (data) {
    var findData = {
        "year": data.toYear,
        "term": data.toTerm
    }
    var result = await ReviewOfCompetenciesLevelModel.find(findData)
    if (result.length === 0) {
        return result;
    } else {
        return DUPLICATE_CODE;
    }
}
exports.cloneValidationCompetencies = async function (data) {
        var findData = {
            "year": data.fromYear,
            "term": data.fromTerm
        }
        var reviewOfCompetenciesLevelarray = [];
        var getReviewOfCompetenciesLevelData = await ReviewOfCompetenciesLevelModel.find(findData);
        getReviewOfCompetenciesLevelData.forEach(elements => {
            var newReviewOfCompetenciesLevelModelData = ReviewOfCompetenciesLevelModel({
                "review_competencies_id": elements.review_competencies_id,
                "competencies_level": elements.competencies_level,
                "weightage": elements.weightage,
                "year": data.toYear,
                "term": data.toTerm,
                "active_status": true
            })
            reviewOfCompetenciesLevelarray.push(newReviewOfCompetenciesLevelModelData)
        })
     
    return reviewOfCompetenciesLevelarray;
}

exports.cloneReviewOfCompetenciesLevel = async function (reviewOfCompetenciesLevelarray) {
    try {
            var resData = await ReviewOfCompetenciesLevelModel.create(reviewOfCompetenciesLevelarray)
            .then((res)=>{
                return "Successfully create a clone - Review of Competencies."
            })
            .catch((err)=>{
                console.log(err)
                return "Error on Bulk clone Creation!"
            })
           return resData;
        } catch (e) {
            throw Error("Error while Creating clone for Review of Competencies.")
        }
}
// clone section end