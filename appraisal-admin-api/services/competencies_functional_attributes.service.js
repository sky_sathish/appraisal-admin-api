var CompetenciesFunctionalAttribute = require('../models/competencies_functional_attributes.model')

// Saving the context of this module inside the _the variable
_this = this

// Async function to get the To do List
exports.getCompetenciesFunctionalAttributes = async function(query, page, limit){

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var competenciesFunctionalAttributes = await CompetenciesFunctionalAttribute.find({})
        .populate("competency_attribute")
        .populate("competency_level")
        .populate("role_id");
        
        // Return the competenciesFunctionalAttributed list that was retured by the mongoose promise
        return competenciesFunctionalAttributes;

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating CompetenciesFunctionalAttributes')
    }
}
exports.getCompetenciesFunctionalAttributesById = async function(id,query, page, limit){

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var competenciesFunctionalAttributes = await CompetenciesFunctionalAttribute.findById(id)
        .populate("competency_attribute")
        .populate("competency_level")
        .populate("role_id");;
        if(competenciesFunctionalAttributes!=undefined){
            return competenciesFunctionalAttributes;
        }else{
            return "Data not found";
        }
        // Return the competenciesFunctionalAttributed list that was retured by the mongoose promise
      

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating CompetenciesFunctionalAttributes')
    }
}

// Async function to get the To do List
exports.getCompetenciesFunctionalAttributesByFunctionIdAndRole = async function(function_id,role_id,query, page, limit){

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }
    
    // Try Catch the awaited promise to handle the error 
    
    try {
        var competenciesFunctionalAttributes = await CompetenciesFunctionalAttribute.find({
            "function_id":function_id,
            "role_id" : role_id
        })
        .populate("competency_attribute")
        .populate("competency_level")
        .populate("role_id");
        
        // Return the competenciesFunctionalAttributed list that was retured by the mongoose promise
        if(competenciesFunctionalAttributes!=undefined && competenciesFunctionalAttributes.length>0){
            return competenciesFunctionalAttributes;
        }else{
            return "Data not found";
        }
      

    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating CompetenciesFunctionalAttributes')
    }
}
exports.createCompetenciesFunctionalAttribute = async function(data){
    
    // Creating a new Mongoose Object by using the new keyword
    var newCompetenciesFunctionalAttributeData = new CompetenciesFunctionalAttribute({
        behavioral_competencies: data.behavioral_competencies,
        competency_level: data.competency_level,
        weightage: data.weightage,
        weightage_unit: data.weightage_unit,
        role_id: data.role_id,
        function_id: data.function_id,
    })

    try{

        // Saving the CompetenciesFunctionalAttribute 
        var savedCompetenciesFunctionalAttribute = await newCompetenciesFunctionalAttributeData.save()
        return savedCompetenciesFunctionalAttribute;
    }catch(e){
        // return a Error message describing the reason     
        throw Error("Error while Creating CompetenciesFunctionalAttribute:"+e.message);
    }
}

exports.updateCompetenciesFunctionalAttribute = async function(data){
    var id = data._id
    try{
        var oldCompetenciesFunctionalAttributeData = await CompetenciesFunctionalAttribute.findById(id);
    }catch(e){
        throw Error("Error occured while Finding the CompetenciesFunctionalAttribute")
    }

    // If no old CompetenciesFunctionalAttribute Object exists return false
    if(!oldCompetenciesFunctionalAttributeData){
        return false;
    }
    //Edit the CompetenciesFunctionalAttribute Object
    oldCompetenciesFunctionalAttributeData.behavioral_competencies=data.behavioral_competencies?data.behavioral_competencies:null;
    oldCompetenciesFunctionalAttributeData.competency_level = data.competency_level?data.competency_level:null,
    oldCompetenciesFunctionalAttributeData.weightage=data.weightage?data.weightage:null;
    oldCompetenciesFunctionalAttributeData.weightage_unit=data.weightage_unit?data.weightage_unit:null;
    oldCompetenciesFunctionalAttributeData.role_id= data.role_id?data.role_id:null;
    oldCompetenciesFunctionalAttributeData.function_id= data.function_id?data.function_id:null;

    try{
        var savedCompetenciesFunctionalAttribute = await oldCompetenciesFunctionalAttributeData.save()
        return savedCompetenciesFunctionalAttribute;
    }catch(e){
        throw Error("And Error occured while updating the CompetenciesFunctionalAttribute:"+e.message);
    }
}

exports.deleteCompetenciesFunctionalAttribute = async function(id){
    
    // Delete the CompetenciesFunctionalAttribute
    try{
        var deleted = await CompetenciesFunctionalAttribute.remove({_id: id})
        if(deleted.n === 0){
            throw Error("CompetenciesFunctionalAttribute Could not be deleted")
        }
        return deleted
    }catch(e){
        throw Error("Error Occured while Deleting the CompetenciesFunctionalAttribute")
    }
}