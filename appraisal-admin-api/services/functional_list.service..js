var FunctionalListModel = require('../models/functional_list.model')

// Saving the context of this module inside the _the variable
_this = this

// Async function to get the To do List
exports.getFunctionalList = async function (query, page, limit) {
   
    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }

    // Try Catch the awaited promise to handle the error 
    
    try {
        var functionalList = await FunctionalListModel.paginate(query, options);
      
        return functionalList;

    } catch (e) {
      
        // return a Error message describing the reason 
        throw Error('Error while Paginating FunctionalList')
    }
}

// Async function to get the To do List
exports.getAllFunctionalList = async function (callback) {

    
    // Try Catch the awaited promise to handle the error 

    try {
        var functionalList = await FunctionalListModel.find();
        
        // Return the roled list that was retured by the mongoose promise

        return functionalList;

    } catch (e) {
       
        // return a Error message describing the reason 
        throw Error('Error while Paginating FunctionalList')
    }
}

exports.getFunctionalListById = async function (id, query, page, limit) {

    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }

    // Try Catch the awaited promise to handle the error 

    try {
        var functionalList = await FunctionalListModel.findById(id);
        if (functionalList != undefined) {
            return functionalList;
        } else {
            return "Data not found";
        }
        // Return the Functional list that was retured by the mongoose promise


    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating FunctionalList')
    }
}
exports.getFunctionalListByName = async function (name, query, page, limit) {

   
    // Options setup for the mongoose paginate
    var options = {
        page,
        limit
    }

    // Try Catch the awaited promise to handle the error 

    try {
        var functionalList = await FunctionalListModel.find({"name":name});
      
        if (functionalList != undefined) {
            return functionalList;
        } else {
            return "Data not found";
        }
        // Return the roled list that was retured by the mongoose promise


    } catch (e) {

        // return a Error message describing the reason 
        throw Error('Error while Paginating FunctionalList')
    }
}


exports.createFunctionalList = async function (functionalList) {

    // Creating a new Mongoose Object by using the new keyword
    var newFunctionalList = new FunctionalListModel({
        name: functionalList.name
    });

    try {
        // Saving the FunctionalList 
        var savedFunctionalList = await newFunctionalList.save()

        return savedFunctionalList;
    } catch (e) {
        // return a Error message describing the reason     
        throw Error("Error while Creating FunctionalList :"+e.message)
    }
}



exports.createFunctionalListBulk = async function (functionalListArr) {

    try {
        
        FunctionalListModel.create(functionalListArr,function (err, docs) {
            if (err){ 
                return console.error(err);
            } else {
              console.log("Multiple documents inserted to FunctionalList");
              return "Multiple documents inserted to FunctionalList";
            }});
      
    } catch (e) {
        throw Error("Error while Creating FunctionalList")
    }
}


exports.updateFunctionalList = async function (functionalList) {
    var id = functionalList.id
    try {
        //Find the old FunctionalList Object by the Id
        var oldFunctionalList = await FunctionalListModel.findById(id);
    } catch (e) {
        throw Error("Error occured while Finding the FunctionalList")
    }

    // If no old FunctionalList Object exists return false
    if (!oldFunctionalList) {
        return false;
    }
    //Edit the FunctionalList Object
    oldFunctionalList.name = functionalList.name

    try {
        var savedFunctionalList = await oldFunctionalList.save()
        return savedFunctionalList;
    } catch (e) {
        throw Error("And Error occured while updating the FunctionalList:"+e.message);
    }
}

exports.deleteFunctionalList = async function (id) {

    // Delete the FunctionalList
    try {
        console.log("id : "+id)
        var deleted = await FunctionalListModel.remove({ _id: id })
        if (deleted.n === 0) {
            throw Error("FunctionalList Could not be deleted")
        }
        return deleted
    } catch (e) {
        throw Error("Error Occured while Deleting the FunctionalList")
    }
}