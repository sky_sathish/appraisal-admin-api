var DevelopmentPlan = require('../models/development_plan.model')
_this = this

exports.getDevelopmentPlan = async function () {
    console.log("inner get")
    try {
        var developmentPlan = await DevelopmentPlan.find({});
        return developmentPlan;
    } catch (e) {
        throw Error('Error in developmentPlan ' + e)
    }
}
exports.getDevelopmentPlanById = async function (id) {
    try {
        var developmentPlan = await DevelopmentPlan.findById(id);
        if (developmentPlan != undefined) {
            return developmentPlan;
        } else {
            return "Data not found";
        }
    } catch (e) {
        throw Error('Error in developmentPlan ' + e)
    }
}
exports.createDevelopmentPlan = async function (req) {
    var newDevelopmentPlan = new DevelopmentPlan({
        employees_career_aspirations: req.body.employees_career_aspirations,
        opportunities_available_within_the_organization: req.body.opportunities_available_within_the_organization,
        training_development_needs: {
            nature_of_training_development_required: req.body.training_development_needs.nature_of_training_development_required,
            action_required: req.body.training_development_needs.action_required,
            by_whom: req.body.training_development_needs.by_whom,
            timeline: req.body.training_development_needs.timeline
        },
        appraisees_comments: req.body.appraisees_comments,
        appraisers_comments: req.body.appraisers_comments,
        appraisees_signature: req.body.appraisees_signature,
        appraisers_signature: req.body.appraisers_signature,
        reviewers_signature: req.body.reviewers_signature
    })
    try {
        var savedDevelopmentPlan = await newDevelopmentPlan.save()
        return savedDevelopmentPlan;
    } catch (e) {
        throw Error("Error in Creating DevelopmentPlan: " + e.message);
    }
}
exports.updateDevelopmentPlan = async function (developmentPlan) {
    var id = developmentPlan.id
    try {
        var oldDevelopmentPlan = await DevelopmentPlan.findById(id);
    } catch (e) {
        throw Error("Error occured while Finding the DevelopmentPlan")
    }
    if (!oldDevelopmentPlan) {
        return false;
    }
    console.log(oldDevelopmentPlan)
    oldDevelopmentPlan.employees_career_aspirations = developmentPlan.employees_career_aspirations,
        oldDevelopmentPlan.opportunities_available_within_the_organization = developmentPlan.opportunities_available_within_the_organization,
        oldDevelopmentPlan.training_development_needs = {
            nature_of_training_development_required: developmentPlan.training_development_needs.nature_of_training_development_required,
            action_required: developmentPlan.training_development_needs.action_required,
            by_whom: developmentPlan.training_development_needs.by_whom,
            timeline: developmentPlan.training_development_needs.timeline
        },
        oldDevelopmentPlan.appraisees_comments = developmentPlan.appraisees_comments,
        oldDevelopmentPlan.appraisers_comments = developmentPlan.appraisers_comments,
        oldDevelopmentPlan.appraisees_signature = developmentPlan.appraisees_signature,
        oldDevelopmentPlan.appraisers_signature = developmentPlan.appraisers_signature,
        oldDevelopmentPlan.reviewers_signature = developmentPlan.reviewers_signature

    console.log("After oldDevelopmentPlan")
    console.log(oldDevelopmentPlan)
    try {
        var savedDevelopmentPlan = await oldDevelopmentPlan.save()
        return savedDevelopmentPlan;
    } catch (e) {
        throw Error("Error occured while updating the DevelopmentPlan: " + e.message);
    }
}
exports.deleteDevelopmentPlan = async function (id) {
    try {
        var deleted = await DevelopmentPlan.remove({ _id: id })
        if (deleted.n === 0) {
            throw Error("DevelopmentPlan Could not be deleted")
        }
        return deleted
    } catch (e) {
        throw Error("Error Occured while Deleting the DevelopmentPlan")
    }
}