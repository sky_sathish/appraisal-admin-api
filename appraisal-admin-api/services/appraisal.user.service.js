var AppraisalUser = require('../models/appraisal.user.model.js');
var AppraisalMaster = require('../models/appraisal.master.model.js');
var mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId;
_this = this

exports.getAppraisalUsers = async function (query, page, limit) {
    console.log("getAppraisalUsers --- ")
    var options = {
        page,
        limit
    }
    try {
        var appraisalUsers = await AppraisalUser.find({});
        return appraisalUsers;
    } catch (e) {
        throw Error('Error while Paginating AppraisalUsers')
    }
}

exports.getAppraisalUserById = async function (id, query, page, limit) {
    console.log("getAppraisalUserById --- " + id)
    var options = {
        page,
        limit
    }
    try {
        var appraisalUser = await AppraisalUser.findById(id);

        if (appraisalUser != undefined) {
            return appraisalUser;
        } else {
            return "Data not found";
        }
    } catch (e) {
        throw Error('Error while Paginating AppraisalUser')
    }
}

// find appraisal user table to find master appraisal id.
exports.getAppraisalById = async function (id, query, page, limit) {
    console.log("getAppraisalById --- " + id)
    var options = {
        page,
        limit
    }
    try {
        var appraisalUser = await AppraisalUser.find({ "appraisal_id": id });
        console.log("-----------------------")
        if (appraisalUser != undefined) {
            return appraisalUser;
        } else {
            return "Data not found";
        }
    } catch (e) {
        throw Error('Error while Paginating AppraisalUser')
    }
}



exports.getAppraisalUserByHashCode = async function (hashCode, query, page, limit) {
    console.log("getAppraisalUserByHashCode --- " + hashCode)
    var options = {
        page,
        limit
    }
    try {
        var appraisalUser = await AppraisalUser.find(
            {
                "hash": hashCode,
                "status": "Active"

            }).populate('appraisal_id');
        console.log("--------------------------")
        // console.log(appraisalUser)
        if (appraisalUser != undefined && appraisalUser.length > 0) {
            return appraisalUser;
        } else {
            return "";
        }
    } catch (e) {
        throw Error('Error while Paginating AppraisalUser')
    }
}


exports.getAppraisalReportingUserByIdAndHashCode = async function (appraisal_id, hashCode) {
    console.log("hashCode")
    console.log(hashCode)
    console.log("appraisal_id")
    console.log(appraisal_id)
    try {
        var appraisalUser = await AppraisalUser.aggregate([
            {
                $match: {
                    "hash": hashCode,
                    "appraisal_id": ObjectId("" + appraisal_id)
                }
            },
            {
                $graphLookup: {
                    from: "appraisal_users",
                    startWith: "$hash",
                    connectFromField: "supervisor_hash",
                    connectToField: "supervisor_hash",
                    as: "reportingHierarchy"
                }
            }
        ]);

        if (appraisalUser != undefined) {
            return appraisalUser;
        } else {
            return "Data not found";
        }
    } catch (e) {
        throw Error('Error while Paginating AppraisalUser')
    }
}


exports.getAppraisalReportingChildByUserIdAndHashCode = async function (appraisal_id, hashCode) {
    console.log("hashCode")
    console.log(hashCode)
    console.log("appraisal_id")
    console.log(appraisal_id)
    try {
        var appraisalUser = await AppraisalUser.aggregate([
            {
                $match: {
                    "supervisor_hash": hashCode,
                    "appraisal_id": ObjectId("" + appraisal_id)
                }
            },
            {
                $graphLookup: {
                    from: "appraisal_users",
                    startWith: "$hash",
                    connectFromField: "supervisor_hash",
                    connectToField: "supervisor_hash",
                    as: "reportingHierarchy"
                }
            }
        ]);

        if (appraisalUser != undefined) {
            return appraisalUser;
        } else {
            return "Data not found";
        }
    } catch (e) {
        throw Error('Error while Paginating AppraisalUser')
    }
}


exports.createAppraisalUser = async function (appraisalUser) {
    console.log("create appraisal master");
    // console.log(appraisalUser);
    var newAppraisalUser = new AppraisalUser({
        appraisal_id: appraisalUser.appraisal_id,
        first_name: appraisalUser.first_name,
        last_name: appraisalUser.last_name,
        emp_code: appraisalUser.emp_code,
        gender: appraisalUser.gender,
        hash: appraisalUser.hash,
        email_id: appraisalUser.email_id,
        group_emp: appraisalUser.group_emp,
        doj: appraisalUser.doj,
        designation: appraisalUser.designation,
        functional_area: appraisalUser.functional_area,
        rg_group: appraisalUser.rg_group,
        supervisor_name: appraisalUser.supervisor_name,
        supervisor_code: appraisalUser.supervisor_code,
        supervisor_hash: appraisalUser.supervisor_hash,
        supervisor_email_id: appraisalUser.supervisor_email_id,
        supervisor_gender: appraisalUser.supervisor_gender,
        supervisor_rg_group: appraisalUser.supervisor_rg_group,
        appraiser_id: appraisalUser.appraiser_id,
        appraiser_name: appraisalUser.appraiser_name,
        reviewer_id: appraisalUser.reviewer_id,
        reviewer_name: appraisalUser.reviewer_name,
        status: appraisalUser.status
    });
    try {
        console.log("---------------------------------------");
        var savedAppraisalUser = await newAppraisalUser.save();
        // console.log(savedAppraisalUser);
        return savedAppraisalUser;
    } catch (e) {
        throw Error("Error while Creating AppraisalUser")
    }
}

exports.createAppraisalUserBulk = async function (appraisalUserarr) {
    console.log("createAppraisalUserBulk");
    try {
        AppraisalUser.create(appraisalUserarr, function (err, docs) {
            if (err) {
                return console.error(err);
            } else {
                console.log("Multiple documents inserted to appraisalUser");
                return "Multiple documents inserted to appraisalUser";
            }
        });

    } catch (e) {
        throw Error("Error while Creating AppraisalUser")
    }
}

exports.updateAppraisalUserBulk = async function (appraisalUserarr) {
    console.log("updateAppraisalUserBulk");

    try {
        for (var i = 0, len = appraisalUserarr.length; i < len; i++) {
            try {
                var oldAppraisalUser = await AppraisalUser.findById(appraisalUserarr[i]._id);
            } catch (e) {
                throw Error("Error occured while Finding the AppraisalUser")
            }
            oldAppraisalUser.appraiser_id = appraisalUserarr[i].appraiser_id,
                oldAppraisalUser.reviewer_id = appraisalUserarr[i].reviewer_id
            oldAppraisalUser.save();
        }
        return "success";
    } catch (e) {
        console.log(e)
        throw Error("Error while Creating AppraisalUser")
    }
}


exports.updateAppraisalUserDetails = async function (appraisalUserDetails) {
    console.log("updateAppraisalUserDetails");
    try {
            var appraisalUserDetailsUpdate = await AppraisalUser.update(
                {
                "appraisal_id" : appraisalUserDetails.appraisal_id,
                "hash" : appraisalUserDetails.employee_id
                },
                {
                    "appraiser_id" : appraisalUserDetails.appraiser_id,
                    "appraiser_name" : appraisalUserDetails.appraiser_name,
                    "reviewer_id" : appraisalUserDetails.reviewer_id,
                    "reviewer_name" : appraisalUserDetails.reviewer_name,
                }, function(err, affected, resp) {
                 });
        
        return appraisalUserDetails.appraisal_id;
    } catch (e) {
        console.log(e)
        throw Error("Error while Creating AppraisalUserDetails")
    }
}


exports.updateAppraisalUser = async function (appraisalUser) {
    var id = appraisalUser.id
    try {
        var oldAppraisalUser = await AppraisalUser.findById(id);
    } catch (e) {
        throw Error("Error occured while Finding the AppraisalUser")
    }
    if (!oldAppraisalUser) {
        return false;
    }
    // console.log(oldAppraisalUser);
    oldAppraisalUser.appraisal_id = appraisalUser.appraisal_id,
        oldAppraisalUser.first_name = appraisalUser.first_name,
        oldAppraisalUser.last_name = appraisalUser.last_name,
        oldAppraisalUser.emp_code = appraisalUser.emp_code,
        oldAppraisalUser.gender = appraisalUser.gender,
        oldAppraisalUser.hash = appraisalUser.hash,
        oldAppraisalUser.email_id = appraisalUser.email_id,
        oldAppraisalUser.group_emp = appraisalUser.group_emp,
        oldAppraisalUser.doj = appraisalUser.doj,
        oldAppraisalUser.designation = appraisalUser.designation,
        oldAppraisalUser.functional_area = appraisalUser.functional_area,
        oldAppraisalUser.rg_group = appraisalUser.rg_group,
        oldAppraisalUser.supervisor_name = appraisalUser.supervisor_name,
        oldAppraisalUser.supervisor_code = appraisalUser.supervisor_code,
        oldAppraisalUser.supervisor_hash = appraisalUser.supervisor_hash,
        oldAppraisalUser.supervisor_email_id = appraisalUser.supervisor_email_id,
        oldAppraisalUser.supervisor_gender = appraisalUser.supervisor_gender,
        oldAppraisalUser.supervisor_rg_group = appraisalUser.supervisor_rg_group,
        oldAppraisalUser.appraiser_id = appraisalUser.appraiser_id,
        oldAppraisalUser.appraiser_name = appraisalUser.appraiser_name,
        oldAppraisalUser.reviewer_id = appraisalUser.reviewer_id,
        oldAppraisalUser.reviewer_name = appraisalUser.reviewer_name,
        oldAppraisalUser.status = appraisalUser.status
    try {
        var savedAppraisalUser = await oldAppraisalUser.save();
        return savedAppraisalUser;
    } catch (e) {
        throw Error("And Error occured while updating the Appraisal User");
    }
}

exports.deleteAppraisalUser = async function (id) {
    try {
        var deleted = await AppraisalUser.remove({
            _id: id
        })
        if (deleted.n === 0) {
            throw Error("AppraisalUser Could not be deleted")
        }
        return deleted;
    } catch (e) {
        throw Error("Error Occured while Deleting the AppraisalUser")
    }
}

