// Accessing the Service that we just created

var GoalCategoryService = require('../services/goal_category.service');

// Saving the context of this module inside the _the variable

_this = this


// Async Controller function to get the To do List

exports.getGoalCategorys = async function(req, res, next){

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000; 
    var id = req.query.id ? req.query.id : undefined;
    try{
    
        var goalCategorys = null;
        if(id != undefined){
            goalCategorys = await GoalCategoryService.getGoalCategorysById(id);
        }else{
            goalCategorys = await GoalCategoryService.getGoalCategorys({}, page, limit);
        }
        if (goalCategorys != null) {
            return res.status(200).json({status: 200, data: goalCategorys, message: "Succesfully GoalCategorys Recieved"});
        } else {
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        
        // Return the goalCategorys list with the appropriate HTTP Status Code and Message.
        
        
        
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}

exports.createGoalCategory = async function(req, res, next){

    // Req.Body contains the form submit values.

    var goalCategory = {
        name: req.body.name
    }

    try{
        
        // Calling the Service function with the new object from the Request Body
    
        var createdGoalCategory = await GoalCategoryService.createGoalCategory(goalCategory)
        return res.status(201).json({status: 201, data: createdGoalCategory, 
            message: "Succesfully Created GoalCategory", success:true})
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, message: goalCategory.name+" is Already Exist",
            success:false})
        }
        return res.status(400).json({status: 400, message: "GoalCategory Creation was Unsuccesfull"})
    }
}

exports.updateGoalCategory = async function(req, res, next){

    // Id is necessary for the update

    if(!req.body._id){
        return res.status(400).json({status: 400., message: "Id must be present"})
    }

    var id = req.body._id;

    console.log(req.body)

    var goalCategory = {
        id,
        name: req.body.name ? req.body.name : null
    }

    try{
        var updatedGoalCategory = await GoalCategoryService.updateGoalCategory(goalCategory)
        return res.status(200).json({status: 200, data: updatedGoalCategory, 
            message: "Succesfully Updated Tod", success:true})
    }catch(e){
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, message: goalCategory.name+" is Already Exist",
            success:false})
        }
        return res.status(400).json({status: 400., message: e.message})
    }
}

exports.removeGoalCategory = async function(req, res, next){

    var id = req.params.id;

    try{
        var deleted = await GoalCategoryService.deleteGoalCategory(id)
        return res.status(204).json({status:204, message: "Succesfully GoalCategory Deleted"})
    }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }

}