// Accessing the Service that we just created

var CompetenciesLevelService = require('../services/competencies_level.service');

// Saving the context of this module inside the _the variable

_this = this


// Async Controller function to get the To do List

exports.getCompetenciesLevel = async function(req, res, next){

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000; 
    var id = req.query.id ? req.query.id : undefined;
    try{
    
        var competencieslevel = null;
        if(id != undefined){
            competencieslevel = await CompetenciesLevelService.getCompetenciesLevelById(id);
        }else{
            competencieslevel = await CompetenciesLevelService.getCompetenciesLevel({}, page, limit);
        }
        if (competencieslevel != null) {
            return res.status(200).json({status: 200, data: competencieslevel, message: "Succesfully competencieslevel Recieved"});
        } else {
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        
        // Return the competencieslevel list with the appropriate HTTP Status Code and Message.
        
        
        
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}

exports.createCompetenciesLevel = async function(req, res, next){

    // Req.Body contains the form submit values.
    
    var competenciesLevel = {
        level_name : req.body.level_name,
        level_desc : req.body.level_desc,
        sequence   : req.body.sequence,
        weightage  : req.body.weightage,
        rating_scale  : req.body.rating_scale,
        scenario_1  : req.body.scenario_1,
        scenario_2  : req.body.scenario_2
    }

    try{
        
        // Calling the Service function with the new object from the Request Body
    
        var createdcompetenciesLevel = await CompetenciesLevelService.createCompetenciesLevel(competenciesLevel)
        return res.status(201).json({status: 201, data: createdcompetenciesLevel, 
            message: "Succesfully Created CompetenciesLevel",success:true})
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, message: competenciesLevel.level_name+" is Already Exist",
            success:false})
        }
        return res.status(400).json({status: 400, message: "CompetenciesLevel Creation was Unsuccesfull"})
    }
}

exports.updateCompetenciesLevel = async function(req, res, next){

    // Id is necessary for the update

    if(!req.body._id){
        return res.status(400).json({status: 400., message: "Id must be present"})
    }

    var id = req.body._id;

    console.log(req.body)

    var competenciesLevel = {
        id,
        level_name : req.body.level_name ? req.body.level_name : null,
        level_desc : req.body.level_desc ? req.body.level_desc : null,
        sequence : req.body.sequence ? req.body.sequence : null,
        weightage : req.body.weightage ? req.body.weightage : null,
        rating_scale  : req.body.rating_scale ? req.body.rating_scale : null,
        scenario_1  : req.body.scenario_1 ? req.body.scenario_1 : null,
        scenario_2  : req.body.scenario_2 ? req.body.scenario_2 : null
    }

    try{
        var updatedCompetenciesLevel = await CompetenciesLevelService.updateCompetenciesLevel(competenciesLevel)
        return res.status(200).json({status: 200, data: updatedCompetenciesLevel, 
            message: "Succesfully Updated Tod",success:true})
    }catch(e){
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, message: competenciesLevel.level_name+" is Already Exist",
            success:false})
        }
        return res.status(400).json({status: 400., message: e.message})
    }
}

exports.removeCompetenciesLevel = async function(req, res, next){

    var id = req.params.id;
console.log("id ----------- "+id)
    try{
        var deleted = await CompetenciesLevelService.deleteCompetenciesLevel(id);
        return res.status(204).json({status:204, message: "Succesfully CompetenciesLevel Deleted"})
    }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }

}