// Accessing the Service that we just created

var GeneralConfigService = require('../services/general_config.service');

// Saving the context of this module inside the _the variable

_this = this


// Async Controller function to get the To do List

exports.getGeneralConfigs = async function(req, res, next){

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000; 
    var id = req.query.id ? req.query.id : undefined;
    try{
    
        var generalConfigs = null;
        if (id != undefined) {
            generalConfigs = await GeneralConfigService.getGeneralConfigsById(id)
        }else{
            generalConfigs = await GeneralConfigService.getGeneralConfigs({}, page, limit)
        }
        if (generalConfigs != null) {
            return res.status(200).json({status: 200, data: generalConfigs, message: "Succesfully GeneralConfigs Recieved"});
        } else {
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        
        // Return the generalConfigs list with the appropriate HTTP Status Code and Message.
        
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}

exports.createGeneralConfig = async function(req, res, next){

    // Req.Body contains the form submit values.

    var generalConfig = {
        key: req.body.key,
        key_desc: req.body.key_desc,
        value: req.body.value,
        tag: req.body.tag ,
        sequence: req.body.sequence, 
        description:req.body.description
    }

    try{
        
        // Calling the Service function with the new object from the Request Body
    
        var createdGeneralConfig = await GeneralConfigService.createGeneralConfig(generalConfig)
        return res.status(201).json({status: 201, data: createdGeneralConfig,
             message: "Succesfully Created GeneralConfig",success:true })
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, message: generalConfig.key+" is Already Exist",
            success:false})
        }
        return res.status(400).json({status: 400, message: "GeneralConfig Creation was Unsuccesfull"})
    }
}

exports.updateGeneralConfig = async function(req, res, next){

    // Id is necessary for the update

    if(!req.body._id){
        return res.status(400).json({status: 400., message: "Id must be present"})
    }

    var id = req.body._id;

    console.log(req.body)

    var generalConfig = {
        id,
        key: req.body.key ? req.body.key : null,
        key_desc: req.body.key_desc ? req.body.key_desc : null,
        value: req.body.value ? req.body.value : null,
        tag: req.body.tag ? req.body.tag : null,
        sequence: req.body.sequence ? req.body.sequence : null,
        description: req.body.description ? req.body.description : null
        

    }

    try{
        var updatedGeneralConfig = await GeneralConfigService.updateGeneralConfig(generalConfig)
        return res.status(200).json({status: 200, data: updatedGeneralConfig,
             message: "Succesfully Updated Tod",success:true })
    }catch(e){
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, message: generalConfig.key+" is Already Exist",
            success:false})
        }
        return res.status(400).json({status: 400., message: e.message})
    }
}

exports.removeGeneralConfig = async function(req, res, next){

    var id = req.params.id;
console.log("id:"+id)
    try{
        var deleted = await GeneralConfigService.deleteGeneralConfig(id)
        return res.status(200).json({status:200, message: "Succesfully GeneralConfig Deleted"})
    }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }

}