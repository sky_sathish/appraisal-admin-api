var ReviewOfCompetenciesLevelService = require('../services/review_of_competencies_level.service');


exports.createReviewOfCompetenciesLevel = async function (req, res, next) {
    try {
        var data = req.body;
        if (data != undefined && data != null) {
            // Calling the Service function with the new object from the Request Body
            var result = await ReviewOfCompetenciesLevelService.createReviewOfCompetenciesLevel(data)
            if (result != null) {
                if (result == 'duplicate') {
                    return res.status(200).json({
                        status: 401,
                        message: "Duplicate entry entered.", success: false
                    })
                } else {
                    return res.status(200).json({
                        status: 200, data: result,
                        message: "Succesfully Created ReviewOfCompetenciesLevel", success: true
                    })
                }
            } else {
                return res.status(500).json({ status: 500, message: "ReviewOfCompetenciesLevel Creation was Unsuccesfull", data: result })
            }
        } else {
            return res.status(500).json({ status: 500, message: "ReviewOfCompetenciesLevel Creation was Unsuccesfull" })
        }
    } catch (e) {
        //Return an Error Response Message with Code and the Error Message.
        return res.status(500).json({ status: 500, message: "ReviewOfCompetenciesLevel Creation was Unsuccesfull " + e.message })
    }
}

exports.updateReviewOfCompetenciesLevel = async function (req, res, next) {

    var data = req.body;
    // Id is necessary for the update
    if (!data._id) {
        return res.status(400).json({ status: 400, message: "Id must be present" })
    }
    try {
        var updatedData = await ReviewOfCompetenciesLevelService.updateReviewOfCompetenciesLevel(data)
        if (updatedData != 'duplicate') {
            return res.status(200)
                .json({ status: 200, data: updatedData, message: "Succesfully Updated ReviewOfCompetenciesLevel", success: true })
        } else {
            return res.status(201)
                .json({ status: 201, data: updatedData, message: "Duplicate entry entered.", success: true })
        }
    } catch (e) {
        return res.status(500).json({ status: 500, message: e.message })
    }
}

exports.removeReviewOfCompetenciesLevel = async function (req, res, next) {

    var id = req.params.id;
    try {
        var data = await ReviewOfCompetenciesLevelService.deleteReviewOfCompetenciesLevel(id);
        if (data != null) {
            return res.status(200).json({ status: 200, message: "success", success: true })
        } else {
            return res.status(400).json({ status: 400, message: "No data found", success: false })
        }

    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message })
    }

}