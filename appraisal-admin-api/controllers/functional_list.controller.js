// Accessing the Service that we just created
var FunctionalListService = require('../services/functional_list.service.');
var AppraisalUserDataService = require('../services/appraisal.users.data.service');
// Async Controller function to get the To do List

exports.getFunctionalList = async function(req, res, next){
    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000; 
    var id = req.query.id ? req.query.id : undefined;
    try{
        var functionalList = null;
        if(id !=undefined){
            functionalList= await FunctionalListService.getFunctionalListById(id);
        }else{
            functionalList= await FunctionalListService.getFunctionalList({}, page, limit);
        }
        if (functionalList != null) {
            return res.status(200).json({status: 200, data: functionalList, message: "Succesfully FunctionalList Recieved"});
        } else {
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
	
    }catch(e){
        //Return an Error Response Message with Code and the Error Message.
        return res.status(400).json({status: 400, message: e.message});
    }
}

exports.getFunctionalListByName = async function(req, res, next){
    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000; 
    var name = req.query.name ? req.query.name : undefined;
 
    try{
        var functionalList = null;
        if(name !=undefined){
            functionalList= await FunctionalListService.getFunctionalListByName(name);
        }else {
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        if (functionalList != null) {
            return res.status(200).json({status: 200, data: functionalList, message: "Succesfully FunctionalList Recieved"});
        } else {
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        
    }catch(e){
        //Return an Error Response Message with Code and the Error Message.
        return res.status(400).json({status: 400, message: e.message});
    }
}

exports.createFunctionalList = async function(req, res, next){
    // Req.Body contains the form submit values.
    var functionalList = {
        name: req.body.name
    }
    try{
        // Calling the Service function with the new object from the Request Body
        var createdFunctionalList = await FunctionalListService.createFunctionalList(functionalList)
        return res.status(200).json({status: 200, data: createdFunctionalList, 
            message: "Succesfully Created FunctionalList", success:true})
    }catch(e){
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, message: "Functional name Already Exist",success:false})
        }
        //Return an Error Response Message with Code and the Error Message.
        return res.status(500).json({status: 500, message: "Functional name Creation was Unsuccesfull " +e.message})
    }
}

exports.autoCreateFunctionalList = async function(req, res, next){
  
    await AppraisalUserDataService.getAppraisalUsers(await function (err, result) {
        if(err){
            return res.status(400).send({
                error: err,
                errMsg: 'Not found',
                success: false
            });
        }else{
           
            var status = true;
            var funListArray = [];
            if (result != undefined && result["EmpList"] != undefined && result["EmpList"]["Emp"] != undefined) {

                result["EmpList"]["Emp"].forEach(element => {
                  var function_name =  element["groupEmp"][0];
                  if(function_name != undefined && function_name != null && function_name != ''){
                    funListArray.push({
                        "name":function_name
                    });
                  }
                });
                try{
                    FunctionalListService.createFunctionalListBulk(funListArray);
                 }catch(e){
                 }
            }else{
                status = false;
            }

            if(status == true){
            return res.status(200).json({
                status: 200, 
                message: "Successfully function list saved.",
                success: true
            });
            }else{
                return  res.status(400).send({
                    status: 400, 
                    message: "Data not found",
                    success: false
                });
            }

        }
    });
}

exports.updateFunctionalList = async function(req, res, next){
    // Id is necessary for the update
    if(!req.body._id){
        return res.status(400).json({status: 400, message: "Id must be present"})
    }
    var id = req.body._id;
    var functionalList = {
        id,
        name: req.body.name ? req.body.name : null
    }
    try{
        var updatedFunctionalList = await FunctionalListService.updateFunctionalList(functionalList);
        return res.status(200)
        .json({status: 200, data: updatedFunctionalList, message: "Succesfully Updated FunctionalList",success:true})
    }catch(e){
        
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, message: "Functional name Already Exist",success:false})
        }
        return res.status(500).json({status: 500, message: e.message})
    }
}

exports.removeFunctionalList = async function(req, res, next){
    var id = req.params.id;
    try{
        var deleted = await FunctionalListService.deleteFunctionalList(id)
        return res.status(204).json({status:204, message: "Succesfully Functional name Deleted"})
    }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }

}