var DevelopmentPlanService = require('../services/development_plan.service');
_this = this
exports.getDevelopmentPlan = async function (req, res, next) {
    console.log("get development plan call")
    var id = req.query.id ? req.query.id : undefined;
    try {

        var developmentPlan = null;
        if (id != undefined) {
            console.log("getDevelopmentPlanById")
            developmentPlan = await DevelopmentPlanService.getDevelopmentPlanById(id);
        } else {
            console.log("getDevelopmentPlan")
            developmentPlan = await DevelopmentPlanService.getDevelopmentPlan();
        }
        if (developmentPlan != null && developmentPlan !=undefined ) {
            return res.status(200).json({ status: 200, data: developmentPlan, message: "Succesfully developmentPlan Recieved" });
        } else {
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
    } catch (e) {
        return res.status(401).json({ status: "Not found", message: "Record not found" });
    }
}

exports.createDevelopmentPlan = async function (req, res, next) {
    console.log("createDevelopmentPlan call")
    try {
        var createdDevelopmentPlan = await DevelopmentPlanService.createDevelopmentPlan(req)
        if(createdDevelopmentPlan != undefined && createdDevelopmentPlan.length > 0){
        return res.status(201).json({
            status: 201, data: createdDevelopmentPlan,
            message: "Succesfully Created DevelopmentPlan",
            success: true
        })
    }
    else{
        return res.status(400).json({ status: 400, message: "DevelopmentPlan Creation was Unsuccesfull" })
    }
    } catch (e) {
        return res.status(400).json({ status: 400, message: "DevelopmentPlan Creation was Unsuccesfull" })
    }
}

exports.updateDevelopmentPlan = async function (req, res, next) {
    console.log("updateDevelopmentPlan call")
    if (!req.body._id) {
        return res.status(400).json({ status: 400., message: "Id must be present" })
    }
    try {
        var updatedDevelopmentPlan = await DevelopmentPlanService.updateDevelopmentPlan(req)
        if(updatedDevelopmentPlan != undefined && updatedDevelopmentPlan.length > 0){
            return res.status(200).json({
                status: 200, data: updatedDevelopmentPlan,
                message: "Succesfully Updated Tod",
                success: true
            })
        }else{
            return res.status(400).json({ status: 400., message: e.message })
        }
    } catch (e) {
        return res.status(400).json({ status: 400., message: e.message })
    }
}

exports.removeDevelopmentPlan = async function (req, res, next) {
    console.log("removeDevelopmentPlan call")
    var id = req.params.id;
    if (!req.params.id) {
        return res.status(400).json({ status: 400., message: "Id must be present" })
    }
    try {
        var deleted = await DevelopmentPlanService.deleteDevelopmentPlan(id)
        console.log(deleted)
        if(deleted.n == 1){
        return res.status(204).json({ status: 204, message: "Succesfully DevelopmentPlan Deleted" })
        }else{
            return res.status(400).json({ status: 400, message: "DevelopmentPlan Unsuccesfully Deleted" })
        }
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message })
    }

}