// Accessing the Service that we just created

var CompetenciesFunctionalAttributeService = require('../services/competencies_functional_attributes.service');

var RoleService = require('../services/role.service');
// Saving the context of this module inside the _the variable

_this = this


// Async Controller function to get the To do List

exports.getCompetenciesFunctionalAttributes = async function(req, res, next){

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000; 
    var id = req.query.id ? req.query.id : undefined;
    var function_id = req.query.function_id ? req.query.function_id : undefined;
    var roleName = req.query.role ? req.query.role : undefined;

    console.log("id : "+id+" | function_id : "+function_id+" | roleName : "+roleName)

    try{
    
        var competenciesFunctionalAttributes = null;
        if(id != undefined && (function_id == undefined && roleName == undefined)) {
            competenciesFunctionalAttributes =await CompetenciesFunctionalAttributeService.getCompetenciesFunctionalAttributesById(id)
            ;
        }else if (id == undefined && (function_id == undefined && roleName == undefined)){
            competenciesFunctionalAttributes =await CompetenciesFunctionalAttributeService.getCompetenciesFunctionalAttributes({}, page, limit);
        }
        else if (id == undefined && (function_id != undefined && roleName != undefined)){
            console.log("stage 3")
            try {
                var roleData = await RoleService.getRolesByName(roleName);
                var role_id = undefined;
                console.log("roleData : "+roleData)
               role_id = roleData[0]['_id'];  
               console.log("role_id : "+role_id)
               if (function_id != undefined && role_id != undefined) {
             
                competenciesFunctionalAttributes=await CompetenciesFunctionalAttributeService.getCompetenciesFunctionalAttributesByFunctionIdAndRole(function_id,role_id,{}, page, limit);
            }else{
                console.log("stage 3.1")
                return res.status(401).json({ status: "Not found", message: "Record not found" });
            }
            } catch (error) {
                return res.status(400).json({status: 400, message: e.message});
            }
           
        }
        else{
            console.log("stage 4")
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        if (competenciesFunctionalAttributes != null) {
            return res.status(200).json({status: 200, data: competenciesFunctionalAttributes, message: "Succesfully CompetenciesFunctionalAttributes Recieved"});
        } else {
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        
        // Return the competenciesFunctionalAttributes list with the appropriate HTTP Status Code and Message.
        
        
        
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}

exports.createCompetenciesFunctionalAttribute = async function(req, res, next){

    // Req.Body contains the form submit values.

    var req_data = req.body;
    try{
        // Calling the Service function with the new object from the Request Body
        var createdCompetenciesFunctionalAttribute = await CompetenciesFunctionalAttributeService.createCompetenciesFunctionalAttribute(req_data)
        return res.status(201).json({status: 201, 
            data: createdCompetenciesFunctionalAttribute,
             message: "Succesfully Created CompetenciesFunctionalAttribute",
             success:true})
    }catch(e){
        //Return an Error Response Message with Code and the Error Message.
        return res.status(400).json({status: 400, message: "CompetenciesFunctionalAttribute Creation was Unsuccesfull"})
    }
}

exports.updateCompetenciesFunctionalAttribute = async function(req, res, next){
    // Id is necessary for the update
    var req_data = req.body;
    if(!req_data._id){
        return res.status(400).json({status: 400., message: "Id must be present"})
    }
    try{
        var updatedCompetenciesFunctionalAttribute = await CompetenciesFunctionalAttributeService.updateCompetenciesFunctionalAttribute(req_data)
        return res.status(200).json({status: 200, data: updatedCompetenciesFunctionalAttribute, 
            message: "Succesfully Updated Tod",
            success:true})
    }catch(e){
        return res.status(400).json({status: 400., message: e.message})
    }
}

exports.removeCompetenciesFunctionalAttribute = async function(req, res, next){

    var id = req.params.id;

    try{
        var deleted = await CompetenciesFunctionalAttributeService.deleteCompetenciesFunctionalAttribute(id)
        return res.status(204).json({status:204, message: "Succesfully CompetenciesFunctionalAttribute Deleted"})
    }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }

}