var ReviewOfCompetenciesService = require('../services/review_of_competencies.service');
var DUPLICATE_CODE= 'duplicate';
// Async Controller function to get the To do List
exports.getReviewOfCompetencies = async function (req, res, next) {

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    var id = req.query.id ? req.query.id : undefined;
    console.log("id : " + id)
    try {
        var result = null;
        if (id != undefined) {
            result = await ReviewOfCompetenciesService.getReviewOfCompetenciesById(id);
        } else {
            // roles= await ReviewOfCompetenciesService.getRoles({}, page, limit);
            result = await ReviewOfCompetenciesService.getAllReviewOfCompetencies();
        }
        if (result != null) {
            return res.status(200).json({ status: 200, data: result, message: "Review of Competencies succesfully Recieved" });
        } else {
            return res.status(401).json({ status: false, message: "Record not found" });
        }
    } catch (e) {
        //Return an Error Response Message with Code and the Error Message.
        return res.status(400).json({ status: 400, message: e.message });
    }
}


exports.createReviewOfCompetencies = async function (req, res, next) {
    try {
        var data = req.body;
        if (data != undefined && data != null) {
            // Calling the Service function with the new object from the Request Body
            var result = await ReviewOfCompetenciesService.createReviewOfCompetencies(data)
            if (result != null) {
                if (result == 'duplicate') {
                    return res.status(200).json({
                        status: 401,
                        message: "Duplicate entry entered.", success: false
                    })
                } else {
                    return res.status(200).json({
                        status: 200, data: result,
                        message: "Succesfully Created ReviewOfCompetencies", success: true
                    })
                }
            } else {
                return res.status(500).json({ status: 500, message: "ReviewOfCompetencies Creation was Unsuccesfull", data: result })
            }
        } else {
            return res.status(500).json({ status: 500, message: "ReviewOfCompetencies Creation was Unsuccesfull" })
        }
    } catch (e) {
        //Return an Error Response Message with Code and the Error Message.
        return res.status(500).json({ status: 500, message: "ReviewOfCompetencies Creation was Unsuccesfull " + e.message })
    }
}

exports.updateReviewOfCompetencies = async function (req, res, next) {

    var data = req.body;
    // Id is necessary for the update
    if (!data._id) {
        return res.status(400).json({ status: 400, message: "Id must be present" })
    }
    try {
        var updatedData = await ReviewOfCompetenciesService.updateReviewOfCompetencies(data)
        return res.status(200)
            .json({ status: 200, data: updatedData, message: "Succesfully Updated ReviewOfCompetencies", success: true })
    } catch (e) {
        return res.status(500).json({ status: 500, message: e.message })
    }
}

exports.removeReviewOfCompetencies = async function (req, res, next) {

    var id = req.params.id;
    try {
        var data = await ReviewOfCompetenciesService.deleteReviewOfCompetencies(id);
        if (data != null) {
            return res.status(200).json({ status: 200, message: "success", success: true })
        } else {
            return res.status(400).json({ status: 400, message: "No data found", success: false })
        }

    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message })
    }

}

exports.cloneReviewOfCompetencies = async function (req, res, next) {
    try {
        var data = req.body;
        if (data != undefined && data != null) {
            if (data.fromTerm != undefined && data.fromTerm != undefined && data.toYear != undefined && data.toTerm != undefined) {
                if (data.fromYear == data.toYear && data.fromTerm == data.toTerm) {
                    return res.status(200).json({ status: 200, data: data, message: "Can't clone same data !!! ", status: false })
                } else {
                    // duplicate checking
                    var dupData = await ReviewOfCompetenciesService.checkDuplicateEntryForBulkInsertReviewOfCompetencies(data)
                    if (dupData == DUPLICATE_CODE) {
                        return res.status(200).json({ status: 200, message: "Duplicate entry!", status: false })
                    }
                    // data already have? or Not? checking
                    var resData = await ReviewOfCompetenciesService.cloneValidationCompetencies(data);
                    if(resData.length == 0){
                        return res.status(200).json({ status: 200, message: "No entry for 'Clone From - year & term' input!", status: false })
                    }
                    // insert bulk clone data
                    var result  = await ReviewOfCompetenciesService.cloneReviewOfCompetenciesLevel(resData);// Questions is same but level is differe.
                    return res.status(200).json({ status: 200, message: result, status: true })
                }
            }
        }
        // default response
        return res.status(200).json({
            status: 200, data: data,
            message: "ReviewOfGoals clone creation was unsuccesfull", success: false
        })

    } catch (e) {
        console.log(e)
        //Return an Error Response Message with Code and the Error Message.
        return res.status(500).json({ status: 500, message: "ReviewOfCompetencies Creation was Unsuccesfull " + e.message })
    }
}