// Accessing the Service that we just created

var MatrixLookForLastYearService = require('../services/matric_look_for_last_year.service');

// Saving the context of this module inside the _the variable

_this = this


// Async Controller function to get the To do List

exports.getMatrixLookForLastYears = async function (req, res, next) {

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value

    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000;
    var id = req.query.id ? req.query.id : undefined;
    
    try {
        var matrixLookForLastYears = null;
        if (id != undefined) {
            matrixLookForLastYears = await MatrixLookForLastYearService.getMatrixsLookForLastYearById(id);
        } else {
            matrixLookForLastYears = await MatrixLookForLastYearService.getMatrixsLookForLastYear({}, page, limit)
        }
        if (matrixLookForLastYears != null) {
            return res.status(200).json({ status: 200, data: matrixLookForLastYears, message: "Succesfully MatrixLookForLastYears Recieved" });
        } else {
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message });
    }
}

exports.createMatrixLookForLastYear = async function (req, res, next) {

    // Req.Body contains the form submit values.

    var matrixLookForLastYear = {
        name: req.body.name
    }

    try {

        // Calling the Service function with the new object from the Request Body

        var createdMatrixLookForLastYear = await MatrixLookForLastYearService.createMatrixLookForLastYear(matrixLookForLastYear)
        return res.status(201).json({ status: 201, data: createdMatrixLookForLastYear, 
            message: "Succesfully Created MatrixLookForLastYear",success:true })
    } catch (e) {

        //Return an Error Response Message with Code and the Error Message.
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, message: matrixLookForLastYear.name+" is Already Exist",success:false})
        }
        return res.status(400).json({ status: 400, 
            message: "MatrixLookForLastYear Creation was Unsuccesfull"})
    }
}

exports.updateMatrixLookForLastYear = async function (req, res, next) {

    // Id is necessary for the update

    if (!req.body._id) {
        return res.status(400).json({ status: 400., message: "Id must be present" })
    }

    var id = req.body._id;

    console.log(req.body)

    var matrixLookForLastYear = {
        id,
        name: req.body.name ? req.body.name : null
    }

    try {
        var updatedMatrixLookForLastYear = await MatrixLookForLastYearService.updateMatrixLookForLastYear(matrixLookForLastYear)
        return res.status(200).json({ status: 200, data: updatedMatrixLookForLastYear, 
            message: "Succesfully Updated Tod",success:true })
    } catch (e) {
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, message: matrixLookForLastYear.name+" is Already Exist",
            success:false})
        }
        return res.status(400).json({ status: 400, message: e.message })
    }
}

exports.removeMatrixLookForLastYear = async function (req, res, next) {

    var id = req.params.id;

    try {
        var deleted = await MatrixLookForLastYearService.deleteMatrixLookForLastYear(id)
        return res.status(204).json({ status: 204, message: "Succesfully MatrixLookForLastYear Deleted" })
    } catch (e) {
        return res.status(400).json({ status: 400, message: e.message })
    }

}