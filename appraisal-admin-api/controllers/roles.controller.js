// Accessing the Service that we just created

var RoleService = require('../services/role.service');
var AppraisalUserDataService = require('../services/appraisal.users.data.service');
// Saving the context of this module inside the _the variable

_this = this


// Async Controller function to get the To do List

exports.getRoles = async function(req, res, next){

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000; 
    var id = req.query.id ? req.query.id : undefined;
    try{
    
        var roles = null;
        if(id !=undefined){
            roles= await RoleService.getRolesById(id);
        }else{
           // roles= await RoleService.getRoles({}, page, limit);
           roles= await RoleService.getAllRoles();
        }
        if (roles != null) {
            return res.status(200).json({status: 200, data: roles, message: "Succesfully Roles Recieved"});
        } else {
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
	// res.setHeader('Access-Control-Allow-Origin', '*');
    // res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    // res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    // res.setHeader('Access-Control-Allow-Credentials', true); 
        
        // Return the roles list with the appropriate HTTP Status Code and Message.
		
        
        
        
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
		
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}


exports.getRolesByName = async function(req, res, next){

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value

    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000; 
    var name = req.query.name ? req.query.name : undefined;
 
    try{
    
        var roles = null;
        if(name !=undefined){
            roles= await RoleService.getRolesByName(name);
        }else {
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        if (roles != null) {
            return res.status(200).json({status: 200, data: roles, message: "Succesfully Roles Recieved"});
        } else {
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
      
        
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
		
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}

exports.createRole = async function(req, res, next){

    // Req.Body contains the form submit values.

    var role = {
        name: req.body.name
        //description: req.body.description,
        //status: req.body.status
    }

    try{
        
        // Calling the Service function with the new object from the Request Body
    
        var createdRole = await RoleService.createRole(role)
        return res.status(201).json({status: 201, data: createdRole, 
            message: "Succesfully Created Role", success:true})
    }catch(e){
        
        
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, message: "Role Already Exist",success:false})
        }
        //Return an Error Response Message with Code and the Error Message.
        
        return res.status(500).json({status: 500, message: "Role Creation was Unsuccesfull " +e.message})
    }
}

exports.autoCreateRole = async function(req, res, next){

    await AppraisalUserDataService.getAppraisalUsers(await function (err, result) {
        if(err){
            return res.status(400).send({
                error: err,
                errMsg: 'Not found',
                success: false
            });
        }else{
           
            var status = true;
            var roleListArray = [];
            if (result != undefined && result["EmpList"] != undefined && result["EmpList"]["Emp"] != undefined) {

                result["EmpList"]["Emp"].forEach(element => {
                  var role_name =  element["designation"][0];
                  if(role_name != undefined && role_name != null && role_name != ''){
                    roleListArray.push({
                        "name":role_name
                    });
                  }
                });
                try{
                    RoleService.createRoleBulk(roleListArray);
                 }catch(e){
                 }
            }else{
                status = false;
            }

            if(status == true){
            return res.status(200).json({
                status: 200, 
                message: "Successfully role list saved.",
                success: true
            });
            }else{
                return  res.status(400).send({
                    status: 400, 
                    message: "Data not found",
                    success: false
                });
            }
        }
    });

}

exports.updateRole = async function(req, res, next){

    // Id is necessary for the update

    if(!req.body._id){
        return res.status(400).json({status: 400, message: "Id must be present"})
    }

    var id = req.body._id;

    console.log(req.body)

    var role = {
        id,
        name: req.body.name ? req.body.name : null
        //description: req.body.description ? req.body.description : null,
        //status: req.body.status ? req.body.status : null
    }

    try{
        var updatedRole = await RoleService.updateRole(role)
        return res.status(200)
        .json({status: 200, data: updatedRole, message: "Succesfully Updated Role",success:true})
    }catch(e){
        
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, message: "Role Already Exist",success:false})
        }
        return res.status(500).json({status: 500, message: e.message})
    }
}

exports.removeRole = async function(req, res, next){

    var id = req.params.id;

    try{
        var data = await RoleService.deleteRole(id);
        if(data != null){
            return res.status(200).json({status:204, data:data, message: "Succesfully Role Deleted"})
        }else{
            return res.status(400).json({status: 400, message: "No data"})
        }
        
    }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }

}