var AppraisalUserService = require('../services/appraisal.user.service.js');
var AppraisalUserDataService = require('../services/appraisal.users.data.service');

_this = this

exports.getAppraisalUser = async function (req, res, next) {
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000;
    var id = req.query.id ? req.query.id : undefined;
    var appraisal_id = req.query.appraisal_id ? req.query.appraisal_id : undefined;
    var hashCode = req.query.uhash ? req.query.uhash : undefined;

    try {
        var appraisalUser = null;
        if ((id != undefined && id != null) && (appraisal_id == undefined && hashCode == undefined)) {
            appraisalUser = await AppraisalUserService.getAppraisalUserById(id);
        }
        else if ((id == undefined && hashCode == undefined) && (appraisal_id != undefined)) {
            appraisalUser = await AppraisalUserService.getAppraisalById(appraisal_id);
        }
        else if (hashCode != undefined && (appraisal_id == undefined && id == undefined)) {
            appraisalUser = await AppraisalUserService.getAppraisalUserByHashCode(hashCode);
        }
        else if ((hashCode != undefined && appraisal_id != undefined) && id == undefined) {
            appraisalUser = await AppraisalUserService.getAppraisalReportingUserByIdAndHashCode(appraisal_id, hashCode);
        } else {
            appraisalUser = await AppraisalUserService.getAppraisalUsers();
        }
        //console.log(appraisalUser)
        if (appraisalUser != null && appraisalUser.length > 0) {
            return res.status(200).json({
                status: 200,
                data: appraisalUser,
                message: "Succesfully AppraisalUser Recieved"
            });
        } else {
            return res.status(200).json({
                status: 200,
                message: "Record not found"
            });
        }
    } catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message
        });
    }
}

exports.getAppraisalChild = async function (req, res, next) {


    var appraisal_id = req.query.appraisal_id ? req.query.appraisal_id : undefined;
    var hashCode = req.query.uhash ? req.query.uhash : undefined;
    var appraisalUser = null;
    var resArray = [];

    if (hashCode != undefined && appraisal_id != undefined) {
        appraisalUser = await AppraisalUserService
            .getAppraisalReportingChildByUserIdAndHashCode(appraisal_id, hashCode);
        console.log(appraisalUser)
        if (appraisalUser != null) {

            appraisalUser.forEach(child => {
                var rep = {
                    name: child["first_name"].toString(),
                    empCode: child["emp_code"].toString(),
                    uhash: child["hash"].toString(),
                    appraisalId: child["appraisal_id"].toString(),
                    hasChildren: false
                }
                if (child != undefined && child != null &&
                    child["reportingHierarchy"] != undefined &&
                    child["reportingHierarchy"].length > 0) {
                    rep["hasChildren"] = true;
                }
                resArray.push(rep)
            });
        }
    }
    return res.status(200).json({
        status: 200,
        data: resArray,
        message: "Succesfully AppraisalUser Recieved"
    });
}

exports.createAppraisalUser = async function (req, res, next) {
    var appraisalUser = {
        appraisal_id: req.body.appraisal_id,
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        emp_code: req.body.emp_code,
        gender: req.body.gender,
        hash: req.body.hash,
        email_id: req.body.email_id,
        group_emp: req.body.group_emp,
        doj: new Date(req.body.doj),
        designation: req.body.designation,
        functional_area: req.body.functional_area,
        rg_group: req.body.rg_group,
        supervisor_name: req.body.supervisor_name,
        supervisor_code: req.body.supervisor_code,
        supervisor_hash: req.body.supervisor_hash,
        supervisor_email_id: req.body.supervisor_email_id,
        supervisor_gender: req.body.supervisor_gender,
        supervisor_rg_group: req.body.supervisor_rg_group,
        appraiser_id: req.body.appraiser_id,
        appraiser_name: req.body.appraiser_name,
        reviewer_id: req.body.reviewer_id,
        reviewer_name: req.body.reviewer_name,
        status: req.body.status
    }
    try {
        var createdAppraisalUser = await AppraisalUserService.createAppraisalUser(appraisalUser);
        return res.status(201).json({
            status: 201,
            data: createdAppraisalUser,
            message: "Succesfully Created AppraisalUser"
        });
    } catch (e) {
        return res.status(400).json({
            status: 400,
            message: "AppraisalUser Creation was Unsuccesfull"
        });
    }
}

exports.updateAppraisalUser = async function (req, res, next) {

    var isBulkUpdate = req.query.bk;
    var isEmpUpdate = req.query.emp;
    if (isBulkUpdate != undefined) {
        try {
            var bulkData = req.body;
            console.log("is bulkData")
            var updatedAppraisalUser = await AppraisalUserService.updateAppraisalUserBulk(bulkData);
            return res.status(200).json({
                status: 200,
                success: updatedAppraisalUser,
                message: "Succesfully Updated Appraisal Master"
            });
        } catch (e) {
            // console.log(e);
            return res.status(400).json({
                status: 400.,
                message: e.message
            });
        }
    }
    else if (isEmpUpdate != undefined) {
        try {
            var empDetails = req.body;
            console.log("is empDetails")
            var updatedAppraisalUser = await AppraisalUserService.updateAppraisalUserDetails(empDetails);
            if (updatedAppraisalUser != undefined) {

                var ResData = await AppraisalUserDataService.updateEmployeeAppraisaldetails(empDetails);
                if (ResData != undefined && ResData != null) {
                    return res.status(200).json({
                        status: 200,
                        result: updatedAppraisalUser,
                        success: true,
                        message: "Succesfully Updated Appraisal User"
                    });
                } else {
                    console.log("400 error")
                    return res.status(400).json({
                        status: 400.,
                        message: "Update operation failed."
                    });
                }
            }
        } catch (e) {
            // console.log(e);
            res.status(400).json({
                status: 400.,
                message: e.message
            });
        }
    }
    else {

        if (!req.body._id) {
            return res.status(400).json({
                status: 400.,
                message: "Id must be present"
            });
        }
        var id = req.body._id;
        // console.log(req.body)
        var appraisalUser = {
            id,
            appraisal_id: req.body.appraisal_id ? req.body.appraisal_id : null,
            first_name: req.body.first_name ? req.body.first_name : null,
            last_name: req.body.last_name ? req.body.last_name : null,
            emp_code: req.body.emp_code ? req.body.emp_code : null,
            gender: req.body.gender ? req.body.gender : null,
            hash: req.body.hash ? req.body.hash : null,
            email_id: req.body.email_id ? req.body.email_id : null,
            group_emp: req.body.group_emp ? req.body.group_emp : null,
            doj: req.body.doj ? new Date(req.body.doj) : null,
            designation: req.body.designation ? req.body.designation : null,
            functional_area: req.body.functional_area ? req.body.functional_area : null,
            rg_group: req.body.rg_group ? req.body.rg_group : null,
            supervisor_name: req.body.supervisor_name ? req.body.supervisor_name : null,
            supervisor_code: req.body.supervisor_code ? req.body.supervisor_code : null,
            supervisor_hash: req.body.supervisor_hash ? req.body.supervisor_hash : null,
            supervisor_email_id: req.body.supervisor_email_id ? req.body.supervisor_email_id : null,
            supervisor_gender: req.body.supervisor_gender ? req.body.supervisor_gender : null,
            supervisor_rg_group: req.body.supervisor_rg_group ? req.body.supervisor_rg_group : null,
            appraiser_id: req.body.appraiser_id ? req.body.appraiser_id : null,
            appraiser_name: req.body.appraiser_name ? req.body.appraiser_name : null,
            reviewer_id: req.body.reviewer_id ? req.body.reviewer_id : null,
            reviewer_name: req.body.reviewer_name ? req.body.reviewer_name : null,
            status: req.body.status ? req.body.status : null
        }

        try {
            var updatedAppraisalUser = await AppraisalUserService.updateAppraisalUser(appraisalUser);
            return res.status(200).json({
                status: 200,
                data: updatedAppraisalUser,
                message: "Succesfully Updated Appraisal Master"
            });
        } catch (e) {
            // console.log(e);
            return res.status(400).json({
                status: 400.,
                message: e.message
            });
        }
    }
}

exports.updateAppraisalUserBulkList = async function (req, res, next) {

    var isBulkUpdate = req.params.bk;

    if (isBulkUpdate != undefined) {
        try {
            var bulkData = req.body;
            console.log(bulkData)
            var updatedAppraisalUser = await AppraisalUserService.createAppraisalUserBulk(bulkData);
            return res.status(200).json({
                status: 200,
                data: updatedAppraisalUser,
                message: "Succesfully Updated Appraisal Master"
            });
        } catch (e) {
            // console.log(e);
            return res.status(400).json({
                status: 400.,
                message: e.message
            });
        }
    } else {
        return res.status(400).json({
            status: 400.,
            message: "It's not bulk data."
        });
    }
}

exports.removeAppraisalUser = async function (req, res, next) {
    var id = req.params.id;
    try {
        var deleted = await AppraisalUserService.deleteAppraisalUser(id)
        return res.status(204).json({
            status: 204,
            message: "Succesfully AppraisalUser Deleted"
        });
    } catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message
        });
    }
}