// Accessing the Service that we just created

var HomeService = require('../services/home.service');



// Saving the context of this module inside the _the variable

_this = this


// Async Controller function to get the To do List

exports.getHomes = async function(req, res, next){

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000; 

    try{
    
        var homes = await HomeService.getHome({}, page, limit)
        
        // Return the homes list with the appropriate HTTP Status Code and Message.
      
        return res.status(200).json({status: 200, data: homes, message: "Succesfully Homes Recieved"});
        
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}

exports.createHome = async function(req, res, next){

    // Req.Body contains the form submit values.

    // var home = {
    //     name: req.body.name
    // }

    // try{
        
    //     // Calling the Service function with the new object from the Request Body
    
    //     var createdHome = await HomeService.createHome(home)
    //     return res.status(201).json({status: 201, data: createdHome, message: "Succesfully Created Home"})
    // }catch(e){
        
    //     //Return an Error Response Message with Code and the Error Message.
        
    //     return res.status(400).json({status: 400, message: "Home Creation was Unsuccesfull"})
    // }
    return res.status(400).json({status: 400, message: "Home Creation was Unsuccesfull"})
}

exports.updateHome = async function(req, res, next){

    // Id is necessary for the update

    // if(!req.body._id){
    //     return res.status(400).json({status: 400., message: "Id must be present"})
    // }

    // var id = req.body._id;

    // console.log(req.body)

    // var home = {
    //     id,
    //     name: req.body.name ? req.body.name : null
    // }

    // try{
    //     var updatedHome = await HomeService.updateHome(home)
    //     return res.status(200).json({status: 200, data: updatedHome, message: "Succesfully Updated Tod"})
    // }catch(e){
    //     return res.status(400).json({status: 400., message: e.message})
    // }
    return res.status(400).json({status: 400., message: e.message})
}

exports.removeHome = async function(req, res, next){

    // var id = req.params.id;

    // try{
    //     var deleted = await HomeService.deleteHome(id)
    //     return res.status(204).json({status:204, message: "Succesfully Home Deleted"})
    // }catch(e){
    //     return res.status(400).json({status: 400, message: e.message})
    // }
    return res.status(400).json({status: 400, message: e.message})

}