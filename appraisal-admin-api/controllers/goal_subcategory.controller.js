// Accessing the Service that we just created

var GoalSubCategoryService = require('../services/goal_subcategory.service');

// Saving the context of this module inside the _the variable

_this = this


// Async Controller function to get the To do List

exports.getGoalSubCategorys = async function(req, res, next){

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000; 
    var id = req.query.id ? req.query.id : undefined;
    try{
    
        var goalSubCategorys = null;
        if( id != undefined){
            goalSubCategorys =  await GoalSubCategoryService.getGoalSubCategoryById(id);
        }else{
            goalSubCategorys =  await GoalSubCategoryService.getGoalSubCategory({}, page, limit);
        }
        if (goalSubCategorys != null) {
            return res.status(200).json({status: 200, data: goalSubCategorys, message: "Succesfully GoalSubCategorys Recieved"});
        } else {
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        
        // Return the goalSubCategorys list with the appropriate HTTP Status Code and Message.
        
        
        
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}

exports.createGoalSubCategory = async function(req, res, next){

    // Req.Body contains the form submit values.
    console.log("------ req.body. name : "+req.body.name);
    var goalSubCategory = {
        name: req.body.name
    }
console.log("------ goalSubCategory name : "+goalSubCategory.name);
    try{
        
        // Calling the Service function with the new object from the Request Body
    
        var createdGoalSubCategory = await GoalSubCategoryService.createGoalSubCategory(goalSubCategory)
        return res.status(201).json({status: 201, data: createdGoalSubCategory,
             message: "Succesfully Created GoalSubCategory",
             success:true})
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        
        return res.status(400).json({status: 400, message: "GoalSubCategory Creation was Unsuccesfull"})
    }
}

exports.updateGoalSubCategory = async function(req, res, next){

    // Id is necessary for the update

    if(!req.body._id){
        return res.status(400).json({status: 400., message: "Id must be present"})
    }

    var id = req.body._id;

    console.log(req.body)

    var goalSubCategory = {
        id,
        name: req.body.name ? req.body.name : null
    }

    try{
        var updatedGoalSubCategory = await GoalSubCategoryService.updateGoalSubCategory(goalSubCategory)
        return res.status(200).json({status: 200, data: updatedGoalSubCategory, 
            message: "Succesfully Updated Tod",
            success:true})
    }catch(e){
        return res.status(400).json({status: 400., message: e.message})
    }
}

exports.removeGoalSubCategory = async function(req, res, next){

    var id = req.params.id;

    try{
        var deleted = await GoalSubCategoryService.deleteGoalSubCategory(id)
        return res.status(204).json({status:204, message: "Succesfully GoalSubCategory Deleted"})
    }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }

}