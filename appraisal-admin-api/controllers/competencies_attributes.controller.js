// Accessing the Service that we just created

var CompetenciesAttributeService = require('../services/competencies_attributes.service');

// Saving the context of this module inside the _the variable

_this = this


// Async Controller function to get the To do List

exports.getCompetenciesAttributes = async function(req, res, next){

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000; 
    var id = req.query.id ? req.query.id : undefined;
    try{
    
        var competenciesAttributes = null;
        if (id != undefined) {
            competenciesAttributes = await CompetenciesAttributeService.getCompetenciesAttributesById(id);
        }else{
        competenciesAttributes = await CompetenciesAttributeService.getCompetenciesAttributes({}, page, limit)
        }
        if (competenciesAttributes != null) {
            return res.status(200).json({status: 200, data: competenciesAttributes, message: "Succesfully CompetenciesAttributes Recieved"});
        }
        else{
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        // Return the competenciesAttributes list with the appropriate HTTP Status Code and Message.
        
        
        
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}

exports.createCompetenciesAttribute = async function(req, res, next){

    // Req.Body contains the form submit values.

    var competenciesAttribute = {
        attribute: req.body.attribute,
        attribute_description: req.body.attribute_description
    }

    try{
        
        // Calling the Service function with the new object from the Request Body
    
        var createdCompetenciesAttribute = await CompetenciesAttributeService.createCompetenciesAttribute(competenciesAttribute)
        return res.status(201).json({status: 201, data: createdCompetenciesAttribute, 
            message: "Succesfully Created CompetenciesAttribute",success:true})
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, 
                message: competenciesAttribute.attribute+" is Already Exist",
            success:false})
        }
        return res.status(400).json({status: 400, message: "CompetenciesAttribute Creation was Unsuccesfull"})
    }
}

exports.updateCompetenciesAttribute = async function(req, res, next){

    // Id is necessary for the update

    if(!req.body._id){
        return res.status(400).json({status: 400., message: "Id must be present"})
    }

    var id = req.body._id;

    console.log(req.body)

    var competenciesAttribute = {
        id,
        attribute: req.body.attribute ? req.body.attribute : null,
        attribute_description: req.body.attribute_description ? req.body.attribute_description : null
    }

    try{
        var updatedCompetenciesAttribute = await CompetenciesAttributeService.updateCompetenciesAttribute(competenciesAttribute)
        return res.status(200).json({status: 200, data: updatedCompetenciesAttribute, 
            message: "Succesfully Updated Tod",success:true})
    }catch(e){
        if(e.message.indexOf("duplicate key error")!=-1){
            return res.status(208).json({status: 208, message: competenciesAttribute.attribute+" is Already Exist",
            success:false})
        }
        return res.status(400).json({status: 400., message: e.message})
    }
}

exports.removeCompetenciesAttribute = async function(req, res, next){

    var id = req.params.id;

    try{
        var deleted = await CompetenciesAttributeService.deleteCompetenciesAttribute(id)
        return res.status(204).json({status:204, message: "Succesfully CompetenciesAttribute Deleted"})
    }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }

}