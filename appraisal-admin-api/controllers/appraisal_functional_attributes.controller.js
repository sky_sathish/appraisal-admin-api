// Accessing the Service that we just created

var ApprisalFunctionalAttributeService = require('../services/appraisal_functional_attributes.service');

var RoleService = require('../services/role.service');
// Saving the context of this module inside the _the variable

_this = this


// Async Controller function to get the To do List

exports.getApprisalFunctionalAttributes = async function(req, res, next){

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000; 
    var id = req.query.id ? req.query.id : undefined;
    var function_id = req.query.function_id ? req.query.function_id : undefined;
    var roleName = req.query.role ? req.query.role : undefined;

    console.log("id : "+id+" | function_id : "+function_id+" | roleName : "+roleName)
    try{
        var apprisalFunctionalAttributes = null;
        if (id != undefined && (function_id == undefined && roleName == undefined)) {
            console.log("stage 1")
            apprisalFunctionalAttributes=await ApprisalFunctionalAttributeService.getApprisalFunctionalAttributesById(id);
        }
        else if (id == undefined && (function_id == undefined && roleName == undefined)){
            console.log("stage 2")
            apprisalFunctionalAttributes= await ApprisalFunctionalAttributeService.getApprisalFunctionalAttributes({}, page, limit);
        }
        else if (id == undefined && (function_id != undefined && roleName != undefined)){
            console.log("stage 3")
            try {
                var roleData = await RoleService.getRolesByName(roleName);
                var role_id = undefined;
        
               role_id = roleData[0]['_id'];  
               if (function_id != undefined && role_id != undefined) {
             
                apprisalFunctionalAttributes=await ApprisalFunctionalAttributeService.getApprisalFunctionalAttributesByRoleAndFunctionID(function_id,role_id,{}, page, limit);
            }else{
                console.log("stage 3.1")
                return res.status(401).json({ status: "Not found", message: "Record not found" });
            }
            } catch (error) {
                return res.status(400).json({status: 400, message: e.message});
            }
           
        }
        else{
            console.log("stage 4")
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        
        if (apprisalFunctionalAttributes != null) {
            return res.status(200).json({status: 200, data: apprisalFunctionalAttributes, message: "Succesfully ApprisalFunctionalAttributes Recieved"});
        }else{
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        // Return the apprisalFunctionalAttributes list with the appropriate HTTP Status Code and Message.
        
       
        
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}


// Async Controller function to get the To do List

exports.getRoleAndFunctionIDApprisalFunctionalAttributes = async function(req, res, next){

    // Check the existence of the query parameters, If the exists doesn't exists assign a default value
    console.log("role : --------------------------------------- ")
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000; 
    var function_id = req.query.function_id ? req.query.function_id : undefined;
    var roleName = req.query.role ? req.query.role : undefined;

    console.log("function_id : "+function_id+" ---- role name : "+roleName)

    try{

        var roleData = await RoleService.getRolesByName(roleName);
        console.log(roleData[0]['_id'])

       var role_id = undefined;

       role_id = roleData[0]['_id'];
      
        var apprisalFunctionalAttributes = null;
        if (function_id != undefined && role_id != undefined) {
            console.log("enter")
            apprisalFunctionalAttributes=await ApprisalFunctionalAttributeService.getApprisalFunctionalAttributesByRoleAndFunctionID(function_id,role_id,{}, page, limit);
        }else{
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        if (apprisalFunctionalAttributes != null) {
            return res.status(200).json({status: 200, data: apprisalFunctionalAttributes, message: "Succesfully ApprisalFunctionalAttributes Recieved"});
        }else{
            return res.status(401).json({ status: "Not found", message: "Record not found" });
        }
        // Return the apprisalFunctionalAttributes list with the appropriate HTTP Status Code and Message.
        
       
        
    }catch(e){
        
        //Return an Error Response Message with Code and the Error Message.
        
        return res.status(400).json({status: 400, message: e.message});
        
    }
}

exports.createApprisalFunctionalAttribute = async function(req, res, next){

    // Req.Body contains the form submit values.

    console.log(req.body)
    var data = req.body;
   
    if(data != undefined && data.function_id != undefined){
        try{
        
            // Calling the Service function with the new object from the Request Body
        
            var createdApprisalFunctionalAttribute = await ApprisalFunctionalAttributeService.createApprisalFunctionalAttribute(data)
            return res.status(201).json({status: 201, data: createdApprisalFunctionalAttribute,
                 message: "Succesfully Created ApprisalFunctionalAttribute",
                 success:true})
        }catch(e){
            //Return an Error Response Message with Code and the Error Message.
            return res.status(400).json({status: 400, message: "ApprisalFunctionalAttribute Creation was Unsuccesfull"})
        }
    }else{
        return res.status(400).json({status: 400, message: "ApprisalFunctionalAttribute Creation was Unsuccesfull. check request data!"}) 
    }
   
}

exports.updateApprisalFunctionalAttribute = async function(req, res, next){

    // Id is necessary for the update
    var req_data = req.body;
    if(!req_data._id){
        return res.status(400).json({status: 400., message: "Id must be present"})
    }

    var id = req_data._id;

    
    console.log(req_data)


    try{
        var updatedApprisalFunctionalAttribute = await ApprisalFunctionalAttributeService.updateApprisalFunctionalAttribute(req_data)
        return res.status(200).json({status: 200, data: updatedApprisalFunctionalAttribute, 
            message: "Succesfully Updated Tod",
            success:true})
    }catch(e){
        return res.status(400).json({status: 400., message: e.message})
    }
}

exports.removeApprisalFunctionalAttribute = async function(req, res, next){

    var id = req.params.id;

    try{
        var deleted = await ApprisalFunctionalAttributeService.deleteApprisalFunctionalAttribute(id)
        return res.status(204).json({status:204, message: "Succesfully ApprisalFunctionalAttribute Deleted"})
    }catch(e){
        return res.status(400).json({status: 400, message: e.message})
    }

}