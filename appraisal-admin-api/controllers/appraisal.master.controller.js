var AppraisalMasterService = require('../services/appraisal.master.service.js');
var AppraisalUserDataService = require('../services/appraisal.users.data.service');
var AppraisalUser = require('../models/appraisal.user.model')
var AppraisalUserService = require('../services/appraisal.user.service.js');
var RoleService = require('../services/role.service');
var RoleModel = require('../models/role.model')
var FunctionalListService = require('../services/functional_list.service.');
var FunctionalListModel = require('../models/functional_list.model')

_this = this

exports.getAppraisalMaster = async function (req, res, next) {
    var page = req.query.page ? req.query.page : 1
    var limit = req.query.limit ? req.query.limit : 10000;
    var id = req.query.id ? req.query.id : undefined;
    try {
        var appraisalMaster = null;
        if (id != undefined && id != null) {
            appraisalMaster = await AppraisalMasterService.getAppraisalMasterById(id);
        } else {
            appraisalMaster = await AppraisalMasterService.getAppraisalMasters();
        }
        if (appraisalMaster != null) {
            return res.status(200).json({
                status: 200,
                data: appraisalMaster,
                message: "Succesfully AppraisalMaster Recieved"
            });
        } else {
            return res.status(200).json({
                status: 200,
                message: "Record not found"
            });
        }
    } catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message
        });
    }
}

exports.createAppraisalMaster = async function (req, res, next) {
    console.log("Controller================");
    // console.log(req.body);
    var appraisalMaster = {
        appraisal_name: req.body.appraisal_name,
        appraisal_intro: req.body.appraisal_intro,
        type: req.body.type,
        due_date: new Date(req.body.due_date),
        status: req.body.status,
        created_date: new Date(req.body.created_date),
        modified_date: new Date(req.body.modified_date),
        created_by: req.body.created_by,
        modified_by: req.body.modified_by
    }
    try {

        var createdAppraisalMaster = await AppraisalMasterService.createAppraisalMaster(appraisalMaster);

        // check createdAppraisalMaster id is null/undefined
        if (createdAppraisalMaster._id != undefined && createdAppraisalMaster._id != null && createdAppraisalMaster._id != "") {

            var intranet_users_role = [];
            var intranet_users_functionalList = [];
            var db_users_roles = [];
            var db_users_functionalList = [];
            var filtered_roles = [];
            var filtered_functionalList = [];

            // get bulk users data from rage intranet portel
            var users = await AppraisalUserDataService.getAppraisalUsers(await function (err, result) {
                if (err) {
                    res.status(402).send({
                        error: err,
                        errMsg: 'Not found',
                        success: false
                    });
                } else {
                    var sendData = [];
                    var newAppraisalUserArr = [];
                    var apprEmployees = {};
                    if (result != undefined && result["EmpList"] != undefined && result["EmpList"]["Emp"] != undefined) {

                        var count = 0;
                        result["EmpList"]["Emp"].forEach(element => {
                            // count++;
                            // if (count >= 3) {
                            //     return false;
                            // }
                            var doj = new Date();
                            if (element["doj"][0] != "" && element["doj"][0] != undefined
                                && element["doj"][0] != null && element["doj"][0] != '0000-00-00') {
                                doj = new Date(element["doj"][0]);
                            }

                            var newAppraisalUser = new AppraisalUser({
                                appraisal_id: createdAppraisalMaster._id,
                                first_name: element["firstName"][0],
                                last_name: element["lastName"][0],
                                emp_code: element["empCode"][0],
                                gender: element["gender"][0],
                                hash: element["hash"][0],
                                email_id: element["emailId"][0],
                                group_emp: element["groupEmp"][0],
                                doj: doj,
                                designation: element["designation"][0],
                                functional_area: element["functionalArea"][0],
                                rg_group: element["rgGroup"][0],
                                supervisor_name: element["reportingToFirstName"][0]+" "+ element["reportingToLastName"][0],
                                supervisor_code: element["reportingTo"][0],
                                supervisor_hash: element["reportingToHash"][0],
                                supervisor_email_id: element["reportingToEmailId"][0],
                                supervisor_gender: element["reportingToGender"][0],
                                supervisor_rg_group: element["reportingTorgGroup"][0],
                                appraiser_id: null,
                                appraiser_name: null,
                                reviewer_id: null,
                                reviewer_name: null,
                                status: "Active",
                            })

                            if (newAppraisalUser["hash"] != undefined) {
                                apprEmployees[newAppraisalUser["hash"]] = {};
                                apprEmployees[newAppraisalUser["hash"]] = newAppraisalUser;
                            }
                            // push appraisal employee details to array
                            newAppraisalUserArr.push(newAppraisalUser);

                            // add employee role in array
                            intranet_users_role.push(newAppraisalUser.designation);
                            // add employee functional id in array
                            intranet_users_functionalList.push(newAppraisalUser.group_emp);
                        });// for each end

                        var countt = 0;
                        //Supervisor loop start
                        newAppraisalUserArr.forEach(ele => {
                            countt++;
                            if (ele != undefined
                                && ele["supervisor_hash"] != undefined
                                && apprEmployees[ele["supervisor_hash"]] != undefined) {

                                var supervisor = apprEmployees[ele["supervisor_hash"]];

                                if (supervisor != undefined
                                    && supervisor["hash"] != undefined
                                    && supervisor["first_name"] != undefined) {
                                    ele["appraiser_id"] = supervisor["hash"];
                                    ele["appraiser_name"] = supervisor["first_name"] + " " + supervisor["last_name"];

                                    var reviewer = apprEmployees[supervisor["supervisor_hash"]];

                                    if (reviewer != undefined
                                        && reviewer["hash"] != undefined
                                        && reviewer["first_name"] != undefined) {
                                        ele["reviewer_id"] = reviewer["hash"];
                                        ele["reviewer_name"] = reviewer["first_name"] + " " + reviewer["last_name"];
                                        ele["reviewer_code"] = reviewer["emp_code"];
                                    }
                                }
                            }
                          
                            // push to mysql data from employee details
                            sendData.push(
                                {
                                    "appraisal_id": ele["appraisal_id"],
                                    "employee_id": ele["hash"],
                                    "employee_name": ele["first_name"] + " " + ele["last_name"],
                                    "employee_code": ele["emp_code"],
                                    "supervisor_id": ele["supervisor_hash"],
                                    "supervisor_name": ele["supervisor_name"],
                                    "supervisor_code": ele["supervisor_code"],
                                    "appraiser_id": ele["appraiser_id"],
                                    "appraiser_name": ele["appraiser_name"],
                                    "reviewer_id": ele["reviewer_id"],
                                    "reviewer_name": ele["reviewer_name"],
                                    "reviewer_code": ele["reviewer_code"],
                                    "level": "",
                                    "function_id": ele["group_emp"],
                                    "role_id": ele["designation"],
                                    "goal_rating": 0,
                                    "competency_rating": 0,
                                    "over_all_performance_rating": 0,
                                    "due_date": appraisalMaster.due_date
                                    
                                });
                        });//Supervisor loop end
                    }// if condition end

                    // bulk appraisaluser data add (mongo)
                    var bulkAppraisalUser = AppraisalUserService.createAppraisalUserBulk(newAppraisalUserArr);
                    // bulk user and appraisal data add (mysql)
                    var bulkEmployeeData = AppraisalUserDataService.addEmployeeAppraisal(sendData);
                }
            });// getAppraisaluser call end

        }// if end


        // role process
        var dbRoles = await RoleService.getAllRoles();

        dbRoles.forEach(element => {
            db_users_roles.push(element.name);
        })

        var finalArray = undefined;

        try {
            finalArray = intranet_users_role.filter(function (ele) {
                return db_users_roles.indexOf(ele) == -1;
            })
        } catch (error) { }

        if (finalArray != undefined && finalArray != "") {
            finalArray.forEach(ele => {
                var newRole = RoleModel({
                    name: ele
                })
                filtered_roles.push(newRole);
            })
            RoleService.createRoleBulk(filtered_roles);
        }

        // functional list process
        var dbFunctionalList = await FunctionalListService.getAllFunctionalList();

        dbFunctionalList.forEach(element => {
            db_users_functionalList.push(element.name);
        })

        var finalArrayFunctionalList = undefined;

        try {
            finalArrayFunctionalList = intranet_users_functionalList.filter(function (ele) {
                return db_users_functionalList.indexOf(ele) == -1;
            })
        } catch (error) { }

        if (finalArrayFunctionalList != undefined && finalArrayFunctionalList != "") {
            finalArrayFunctionalList.forEach(ele => {
                var newFunctionalList = FunctionalListModel({
                    name: ele
                })
                filtered_functionalList.push(newFunctionalList);
            })
            FunctionalListService.createFunctionalListBulk(filtered_functionalList);
        }
        return res.status(201).json({
            status: 201,
            result: createdAppraisalMaster,
            message: "Succesfully Created AppraisalMaster", success: true
        });
    } catch (e) {
        if (e.message.indexOf("duplicate key error") != -1) {
            return res.status(208).json({
                status: 208, message: "'" + appraisalMaster.appraisal_name + "' is Already Exist",
                success: false
            })
        }
        return res.status(400).json({
            status: 400,
            message: "AppraisalMaster Creation was Unsuccesfull" + e
        });
    }
}

exports.updateAppraisalMaster = async function (req, res, next) {
    if (!req.body._id) {
        return res.status(400).json({
            status: 400.,
            message: "Id must be present"
        })
    }
    var id = req.body._id;
    console.log(req.body)
    var appraisalMaster = {
        id,
        appraisal_name: req.body.appraisal_name ? req.body.appraisal_name : null,
        appraisal_intro: req.body.appraisal_intro ? req.body.appraisal_intro : null,
        type: req.body.type ? req.body.type : null,
        due_date: req.body.due_date ? req.body.due_date : null,
        status: req.body.status ? req.body.status : null,
        created_date: req.body.created_date ? req.body.created_date : null,
        modified_date: req.body.modified_date ? req.body.modified_date : null,
        created_by: req.body.created_by ? req.body.created_by : null,
        modified_by: req.body.modified_by ? req.body.modified_by : null,
        islocked: req.body.islocked ? req.body.islocked : false
    }

    try {
        var updatedAppraisalMaster = await AppraisalMasterService.updateAppraisalMaster(appraisalMaster);
        return res.status(200).json({
            status: 200,
            data: updatedAppraisalMaster,
            message: "Succesfully Updated Appraisal Master", success: true
        })
    } catch (e) {
        if (e.message.indexOf("duplicate key error") != -1) {
            return res.status(208).json({
                status: 208, message: "'" + appraisalMaster.appraisal_name + "' is Already Exist",
                success: false
            })
        }
        return res.status(400).json({
            status: 400.,
            message: e.message
        })
    }
}

exports.removeAppraisalMaster = async function (req, res, next) {
    var appraisal_id = req.params.id;
    console.log("removeAppraisalMaster : " + appraisal_id)
    try {
        var deleted = await AppraisalMasterService.deleteAppraisalMaster(appraisal_id)
        return res.status(204).json({
            status: 204,
            message: "Succesfully AppraisalMaster Deleted"
        })
    } catch (e) {
        return res.status(400).json({
            status: 400,
            message: e.message
        })
    }
}