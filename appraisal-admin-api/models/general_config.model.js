var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')


var GeneralConfigSchema = new mongoose.Schema({
    key: { type : String , unique : true, required : true, dropDups: true },
    key_desc:String,
    value:String,
    tag:String,
    sequence:Number,
    description:String
}, { collection: 'general_config' })

GeneralConfigSchema.plugin(mongoosePaginate)
const GeneralConfig = mongoose.model('GeneralConfig', GeneralConfigSchema)

module.exports = GeneralConfig;