var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var ApprisalFunctionalAttributeSchema = new mongoose.Schema({
    function_id:String,
    role_id:{type: Schema.Types.ObjectId, ref: 'Role' },
    category:{type: Schema.Types.ObjectId, ref: 'GoalCategory'},
    subcategory:{type: Schema.Types.ObjectId, ref: 'GoalSubCategory'},
    description:String,
    target_for_last_year:String,
    target_for_last_year_unit:String,
    target_for_last_year_description:String,
    target_for_next_year:String,
    target_for_next_year_unit:String,
    target_for_next_year_description:String,
    weightage:Number
},{collection:"appraisal_functional_attributes"})

ApprisalFunctionalAttributeSchema.plugin(mongoosePaginate)
const ApprisalFunctionalAttribute = mongoose.model('ApprisalFunctionalAttribute', ApprisalFunctionalAttributeSchema)

module.exports = ApprisalFunctionalAttribute;