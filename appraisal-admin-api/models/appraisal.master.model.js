var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;


var AppraisalMasterSchema = new mongoose.Schema({
    appraisal_name: { type : String , unique : true, required : true, dropDups: true },
    appraisal_intro: String,
    type: Number,
    due_date: Date,
    status: String,
    created_date: Date,
    modified_date: Date,
    created_by: String,
    modified_by: String,
    islocked:Boolean
}, {
    collection: "appraisal_masters"
});

AppraisalMasterSchema.plugin(mongoosePaginate);
const ApprisalMaster = mongoose.model('ApprisalMaster', AppraisalMasterSchema);

module.exports = ApprisalMaster;