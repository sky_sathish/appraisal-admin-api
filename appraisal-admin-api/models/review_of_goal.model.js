var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var ReviewOfGoalsSchema = new mongoose.Schema({
    function:{ type : String , required : true},
    role: {type: Schema.Types.ObjectId, ref: 'Role', required : true },
    category:{type: Schema.Types.ObjectId, ref: 'GoalCategory', required : true },
    subcategory:{type: Schema.Types.ObjectId, ref: 'GoalSubCategory', required : true },
    description:{ type : String , required : true}
    
},{collection:"review_of_goals"})

ReviewOfGoalsSchema.plugin(mongoosePaginate)
const ReviewOfGoals = mongoose.model('ReviewOfGoals', ReviewOfGoalsSchema)

module.exports = ReviewOfGoals;