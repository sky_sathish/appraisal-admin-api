var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var DevelopmentPlanSchema = new mongoose.Schema({

    employees_career_aspirations: String,
    opportunities_available_within_the_organization: String,
    training_development_needs: {
        nature_of_training_development_required: String,
        action_required: String,
        by_whom: String,
        timeline: String
    },
    appraisees_comments: String,
    appraisers_comments: String,
    appraisees_signature: String,
    appraisers_signature: String,
    reviewers_signature: String
}, { collection: "development_plan" })

DevelopmentPlanSchema.plugin(mongoosePaginate)
const DevelopmentPlan = mongoose.model('DevelopmentPlan', DevelopmentPlanSchema)

module.exports = DevelopmentPlan;