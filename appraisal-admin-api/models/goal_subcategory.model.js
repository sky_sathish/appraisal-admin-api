var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')


var GoalSubCategorySchema = new mongoose.Schema({
    name  : { type : String , required : true}
}, { collection: 'goal_subcategory' })

GoalSubCategorySchema.plugin(mongoosePaginate)
const GoalSubCategory = mongoose.model('GoalSubCategory', GoalSubCategorySchema)

module.exports = GoalSubCategory;