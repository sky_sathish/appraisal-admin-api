var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var ReviewOfCompetenciesSchema = new mongoose.Schema({
    function:{ type : String , required : true},
    role: {type: Schema.Types.ObjectId, ref: 'Role', required : true },
    description:{ type : String , required : true}
    
},{collection:"review_of_competencies"})

ReviewOfCompetenciesSchema.plugin(mongoosePaginate)
const ReviewOfCompetencies = mongoose.model('ReviewOfCompetencies', ReviewOfCompetenciesSchema)

module.exports = ReviewOfCompetencies;