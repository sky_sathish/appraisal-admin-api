var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;


var GoalAttributeSchema = new mongoose.Schema({
    attribute:String,
    attribute_category:{type: Schema.Types.ObjectId, ref: 'GoalCategory' },
    attribute_sub_category:{type: Schema.Types.ObjectId, ref: 'GoalSubCategory' }
},{ collection: 'goal_attributes' })

GoalAttributeSchema.plugin(mongoosePaginate)
const GoalAttribute = mongoose.model('GoalAttribute', GoalAttributeSchema)

module.exports = GoalAttribute;