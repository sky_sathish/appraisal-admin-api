var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')


var CompetenciesLevelSchema = new mongoose.Schema({
    level_name:{ type : String , unique : true, required : true, dropDups: true },
    level_desc:String,
    sequence:String,
    weightage:String,
    rating_scale :String,
    scenario_1:String,
    scenario_2:String

}, { collection: 'competencies_level' })

CompetenciesLevelSchema.plugin(mongoosePaginate)
const CompetenciesLevel = mongoose.model('CompetenciesLevel', CompetenciesLevelSchema)

module.exports = CompetenciesLevel;