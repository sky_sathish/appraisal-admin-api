var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')


var CompetenciesAttributeSchema = new mongoose.Schema({
    attribute:{ type : String , required : true, dropDups: true },
    attribute_description:String
}, { collection: 'competencies_attributes' })

CompetenciesAttributeSchema.plugin(mongoosePaginate)
const CompetenciesAttribute = mongoose.model('CompetenciesAttribute', CompetenciesAttributeSchema)

module.exports = CompetenciesAttribute;