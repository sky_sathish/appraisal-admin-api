var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')


var MatrixLookForLastYearSchema = new mongoose.Schema({
    name  : { type : String , unique : true, required : true, dropDups: true }
}, { collection: 'matric_look_for_last_year' })

MatrixLookForLastYearSchema.plugin(mongoosePaginate)
const MatrixLookForLastYear = mongoose.model('MatrixLookForLastYear', MatrixLookForLastYearSchema)

module.exports = MatrixLookForLastYear;