var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var CompetenciesFunctionalAttributesSchema = new mongoose.Schema({
    behavioral_competencies:String,
    competency_level:{type: Schema.Types.ObjectId, ref: 'CompetenciesLevel' },
    weightage:Number,
    weightage_unit:String,
    role_id:{type: Schema.Types.ObjectId, ref: 'Role' },
    function_id:String
}, { collection: 'competencies_functional_attributes' })

CompetenciesFunctionalAttributesSchema.plugin(mongoosePaginate)
const CompetenciesFunctionalAttributes = mongoose.model('CompetenciesFunctionalAttributes', CompetenciesFunctionalAttributesSchema)

module.exports = CompetenciesFunctionalAttributes;