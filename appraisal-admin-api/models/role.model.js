var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')

var RoleSchema = new mongoose.Schema(
	{
		name  : { type : String , unique : true, required : true, dropDups: true }
	},
	{ collection: 'roles' }
)

RoleSchema.plugin(mongoosePaginate)
const Role = mongoose.model('Role', RoleSchema)

module.exports = Role;