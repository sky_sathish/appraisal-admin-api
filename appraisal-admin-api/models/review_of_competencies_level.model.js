var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var ReviewOfCompetenciesLevelSchema = new mongoose.Schema({
    review_competencies_id: {type: Schema.Types.ObjectId, ref: 'ReviewOfGoals', required : true },
    competencies_level:{type: Schema.Types.ObjectId, ref: 'CompetenciesLevel' , required : true },
    weightage:{ type : String , required : true},
    year:{ type : String , required : true},
    term:{ type : String , required : true},
    active_status:{ type : Boolean , required : true}
},{collection:"review_of_competencies_level"})

ReviewOfCompetenciesLevelSchema.plugin(mongoosePaginate)
const ReviewOfCompetenciesLevel = mongoose.model('ReviewOfCompetenciesLevel', ReviewOfCompetenciesLevelSchema)

module.exports = ReviewOfCompetenciesLevel;