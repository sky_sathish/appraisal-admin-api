var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')


var GoalCategorySchema = new mongoose.Schema({
    name  : { type : String , unique : true, required : true, dropDups: true }
}, { collection: 'goal_category' })

GoalCategorySchema.plugin(mongoosePaginate)
const GoalCategory = mongoose.model('GoalCategory', GoalCategorySchema)

module.exports = GoalCategory;