var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;


var AppraisalUserSchema = new mongoose.Schema({
    appraisal_id: { type: Schema.Types.ObjectId, ref: 'ApprisalMaster' },
    first_name: String,
    last_name: String,
    emp_code: String,
    gender: String,
    hash: String,
    email_id: String,
    group_emp: String,
    doj: Date,
    designation: String,
    functional_area: String,
    rg_group: String,
    supervisor_name: String,
    supervisor_code: String,
    supervisor_hash: String,
    supervisor_email_id: String,
    supervisor_gender: String,
    supervisor_rg_group: String,
    appraiser_id: String,
    appraiser_name: String,
    reviewer_id: String,
    reviewer_name: String,
    status: String

}, {
        collection: "appraisal_users"
    });

AppraisalUserSchema.plugin(mongoosePaginate);
const ApprisalUser = mongoose.model('ApprisalUser', AppraisalUserSchema);

module.exports = ApprisalUser;