var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')


var FunctionalListSchema = new mongoose.Schema(
	{
		name  : { type : String , unique : true, required : true, dropDups: true }
	},
	{ collection: 'functional_list' }
);

	FunctionalListSchema.plugin(mongoosePaginate)
const FunctionalList = mongoose.model('Functional_List', FunctionalListSchema)

module.exports = FunctionalList;