var mongoose = require('mongoose')
var mongoosePaginate = require('mongoose-paginate')
var Schema = mongoose.Schema;

var ReviewOfGoalsTargetSchema = new mongoose.Schema({
    review_goals_id: {type: Schema.Types.ObjectId, ref: 'ReviewOfGoals', required : true },
    target:{ type : String , required : true},
    weightage:{ type : String , required : true},
    year:{ type : String , required : true},
    term:{ type : String , required : true},
    active_status:{ type : Boolean , required : true}
},{collection:"review_of_goals_target"})

ReviewOfGoalsTargetSchema.plugin(mongoosePaginate)
const ReviewOfGoalsTarget = mongoose.model('ReviewOfGoalsTarget', ReviewOfGoalsTargetSchema)

module.exports = ReviewOfGoalsTarget;